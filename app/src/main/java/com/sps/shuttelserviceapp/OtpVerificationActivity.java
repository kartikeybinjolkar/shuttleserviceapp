package com.sps.shuttelserviceapp;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.sps.shuttelserviceapp.AutoOtpDetection.SmsListener;
import com.sps.shuttelserviceapp.AutoOtpDetection.SmsReceiver;
import com.sps.shuttelserviceapp.InternetCheck.ConnectionReceiver;
import com.sps.shuttelserviceapp.NotificationService.NotificationMsgService;
import com.sps.shuttelserviceapp.NotificationService.SharedPreferencesManager;
import com.sps.shuttelserviceapp.Utils.GlobalVariables;
import com.sps.shuttelserviceapp.Utils.ViewDialog;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class OtpVerificationActivity extends AppCompatActivity implements View.OnClickListener, ConnectionReceiver.ReceiverListener{

    boolean isConnected;
    Button btn_submit;
    EditText et_otp1,et_otp2,et_otp3,et_otp4;
    String verify_input_json;
    JSONObject validate_otp_response;
    TextView tv_resend_otp_timer,tv_resend_otp;
    JSONObject resend_otp_response;
    String resend_otp_input_json;
    
    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_verification);

        btn_submit = findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(this);

        et_otp1 = findViewById(R.id.et_otp1);
        et_otp2 = findViewById(R.id.et_otp2);
        et_otp3 = findViewById(R.id.et_otp3);
        et_otp4 = findViewById(R.id.et_otp4);

        tv_resend_otp_timer= (TextView) findViewById(R.id.tv_resend_otp_timer);
        tv_resend_otp= (TextView) findViewById(R.id.tv_resend_otp);
        tv_resend_otp.setEnabled(false);
        tv_resend_otp.setAlpha(0.4f);
        tv_resend_otp.setOnClickListener(this);

        SmsReceiver.bindListener(new SmsListener() {
            @Override
            public void messageReceived(String messageText) {
                Log.e("Auto Detected OTP", messageText);
                char[] ch = new char[messageText.length()];
                for (int i = 0; i < messageText.length(); i++) {
                    ch[i] = messageText.charAt(i);

                }
                String otp1 = String.valueOf(ch[0]);
                String otp2 = String.valueOf(ch[1]);
                String otp3 = String.valueOf(ch[2]);
                String otp4 = String.valueOf(ch[3]);
                et_otp1.setText(otp1);
                et_otp2.setText(otp2);
                et_otp3.setText(otp3);
                et_otp4.setText(otp4);

            }
        });

        ResendOTPTimer();

        StringBuilder sb=new StringBuilder();

        et_otp1.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if(sb.length()==0&et_otp1.length()==1)
                {
                    sb.append(s);
                    et_otp1.clearFocus();
                    et_otp2.requestFocus();
                    et_otp2.setCursorVisible(true);

                }


            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

                if(sb.length()==1)
                {

                    sb.deleteCharAt(0);

                }

            }

            public void afterTextChanged(Editable s) {
                if(sb.length()==0)
                {

                    et_otp1.requestFocus();
                }

            }
        });

        et_otp2.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if(sb.length()==0&et_otp2.length()==1)
                {
                    sb.append(s);
                    et_otp2.clearFocus();
                    et_otp3.requestFocus();
                    et_otp3.setCursorVisible(true);

                }


            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

                if(sb.length()==1)
                {

                    sb.deleteCharAt(0);

                }

            }

            public void afterTextChanged(Editable s) {
                if(sb.length()==0)
                {

                    et_otp2.requestFocus();
                }

            }
        });

        et_otp3.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if(sb.length()==0&et_otp3.length()==1)
                {
                    sb.append(s);
                    et_otp3.clearFocus();
                    et_otp4.requestFocus();
                    et_otp4.setCursorVisible(true);

                }


            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

                if(sb.length()==1)
                {

                    sb.deleteCharAt(0);

                }

            }

            public void afterTextChanged(Editable s) {
                if(sb.length()==0)
                {

                    et_otp3.requestFocus();
                }

            }
        });

        et_otp4.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                if(sb.length()==0&et_otp4.length()==1)
                {
                    sb.append(s);

                }


            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

                if(sb.length()==1)
                {

                    sb.deleteCharAt(0);

                }

            }

            public void afterTextChanged(Editable s) {
                if(sb.length()==0)
                {

                    et_otp4.requestFocus();
                }

            }
        });
    }



    @Override
    public void onClick(View v) {
        switch(v.getId()) {

            case R.id.tv_resend_otp:
                tv_resend_otp.setEnabled(false);
                tv_resend_otp.setAlpha(0.4f);
                et_otp1.getText().clear();
                et_otp1.requestFocus();
                et_otp2.getText().clear();
                et_otp3.getText().clear();
                et_otp4.getText().clear();
                et_otp2.clearFocus();
                et_otp3.clearFocus();
                et_otp4.clearFocus();
                et_otp1.setCursorVisible(true);
                String resent_otp_url = GlobalVariables.API_URL + "worker/generate-otp";

                resend_otp_input_json = "{\n" +
                        "  \"mobile\": \""+GlobalVariables.phone_number+"\",\n" +
                        "  \"country_code_id\": \""+GlobalVariables.country_code+"\"\n" +
                        "}";

                GlobalVariables.progressDialog = GlobalVariables.createProgressDialog(OtpVerificationActivity.this);
                GlobalVariables.progressDialog.show();

                Thread thread = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try {
                            ResendOTP(resent_otp_url, resend_otp_input_json);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                });
                thread.start();

                ResendOTPTimer();

                break;

            case R.id.btn_submit:

                if(et_otp1.getText().toString().equals("")|| et_otp2.getText().toString().equals("")
                        ||et_otp3.getText().toString().equals("")||et_otp4.getText().toString().equals("")){
                    ViewDialog alert = new ViewDialog();
                    alert.showDialog(OtpVerificationActivity.this, "Please Enter 4 digit OTP");
                }
                else{
                    checkConnection();
                    if(isConnected){

                        String url = GlobalVariables.API_URL + "supervisor/signin";
                        String OTP = et_otp1.getText().toString()+et_otp2.getText().toString()+et_otp3.getText().toString()+et_otp4.getText().toString();

                        verify_input_json = "{\n" +
                                "  \"mobile\": \""+GlobalVariables.phone_number+"\",\n" +
                                "  \"country_code_id\": \""+GlobalVariables.country_code+"\",\n" +
                                "  \"otp\": \""+OTP+"\"\n" +
                                "}";

                        GlobalVariables.progressDialog = GlobalVariables.createProgressDialog(OtpVerificationActivity.this);
                        GlobalVariables.progressDialog.show();

                        Thread thread_one = new Thread(new Runnable() {

                            @Override
                            public void run() {
                                try {
                                    ValidateOTP(url, verify_input_json);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        });
                        thread_one.start();

                    }else{

                        Log.e("Checking 2 ", String.valueOf(isConnected));
                        Toast.makeText(this,"Please Check your Internet Connection !",Toast.LENGTH_LONG).show();

                    }


                }
        }
    }

    public JSONObject ValidateOTP(String url, String json) {

        Log.e("sign_in API Json",json);
        JSONObject jsonObjectResp = null;

        try {

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, json);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .method("POST",body)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                jsonObjectResp = ValidateOTPResponse(networkResp);
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            jsonObjectResp = ValidateOTPResponse(err);
        }

        return jsonObjectResp;
    }

    private JSONObject ValidateOTPResponse(final String str) {

        String id, name;
        validate_otp_response = null;
        try {
            validate_otp_response = new JSONObject(str);
            Log.e("user profile Response=>", String.valueOf(validate_otp_response));

            String message = validate_otp_response.getString("msg");
            int code = validate_otp_response.getInt("code");


            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (code == 20) {

                        try {
                            JSONObject json = new JSONObject(str);

                            JSONObject jsonObject = json.getJSONObject("data");
                            String user_name = jsonObject.getString("first_name");
                            String token = jsonObject.getString("token");
                            String site_id = jsonObject.getString("sites");
                            Log.e("User Token",token);
                            String role_id = jsonObject.getString("role_id");
                            String mobile = jsonObject.getString("mobile");
                            int vas_type_id = jsonObject.getInt("vas_type_id");
                            GlobalVariables.siteid = site_id;
                            GlobalVariables.user_name = user_name;
                            GlobalVariables.user_role_id = role_id;
                            GlobalVariables.vas_type_id = vas_type_id;
                            GlobalVariables.user_token =token;

                                JSONObject work_details = jsonObject.getJSONObject("work_details");
                                String assigned_works = work_details.getString("assigned_works");
                                String pending_works = work_details.getString("pending_works");

                                GlobalVariables.assigned_works = assigned_works;
                                GlobalVariables.pending_works = pending_works;

                                SharedPreferences prefs = getSharedPreferences(
                                        "saved_data", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = prefs.edit();
                                editor.putString("login_status", "log_in");
                                editor.putString("token", token);
                                editor.putString("role_id", role_id);
                                editor.putString("user_mobile", mobile);
                                editor.putString("user_name", user_name);
                                editor.putString("site_id", site_id);
                                editor.apply();

                            SharedPreferencesManager.init(OtpVerificationActivity.this);
                            NotificationMsgService notifMsgServ = new NotificationMsgService();
                            Log.i("Notification","Call after Login");
                            notifMsgServ.returnToken(GlobalVariables.phone_number);

                            if (vas_type_id == 3) {
                                Intent submit = new Intent(OtpVerificationActivity.this, OrderListActivity.class);
                                startActivity(submit);
                                finish();
                            }else{
                                ViewDialog alert = new ViewDialog();
                                alert.showDialog(OtpVerificationActivity.this, "Not a Valid User");
                                SharedPreferences prefs1 = getSharedPreferences(
                                        "saved_data", Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor1 = prefs1.edit();
                                editor1.putString("login_status", "");
                                editor1.putString("token", "");
                                editor1.putString("role_id", "");
                                editor1.putInt("vas_type_id", 0);
                                editor1.putString("user_name", "");
                                editor1.apply();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    } else {
                        ViewDialog alert = new ViewDialog();
                        alert.showDialog(OtpVerificationActivity.this, message);
                        // Toast.makeText(OTPVerificationActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                    }

                    GlobalVariables.progressDialog.dismiss();

                }
            });


        } catch (Exception ex) {
            //  Log.e("Could not parse malformed JSON: \"" + json + "\"");
            try {
                validate_otp_response = new JSONObject();
                validate_otp_response.put("result", "failed");
                validate_otp_response.put("data", str);
                validate_otp_response.put("error", ex.getMessage());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        // tv_response.setText("ID:"+id+" and Name:"+name);
                        Toast.makeText(OtpVerificationActivity.this, "" + ex.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
            } catch (Exception exx) {
            }
            GlobalVariables.progressDialog.dismiss();
        }

        return validate_otp_response;
    }

    public JSONObject ResendOTP(String url, String json) {

        Log.e("send_otp_url=>",url);
        Log.e("send_otp_json=>",json);
        String Header_id,Header_value;


        JSONObject jsonObjectResp = null;

        try {

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, json);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .method("POST",body)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                jsonObjectResp = ResendOTPResponse(networkResp);
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            jsonObjectResp = ResendOTPResponse(err);
        }

        return jsonObjectResp;
    }

    public JSONObject ResendOTPResponse(final String str) {
        String id, name;
        resend_otp_response = null;
        try {
            resend_otp_response = new JSONObject(str);
            Log.e("resend OTP Response=>", String.valueOf(resend_otp_response));

            String message = resend_otp_response.getString("msg");
            int code = resend_otp_response.getInt("code");


            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (code == 20) {

                        Toast.makeText(OtpVerificationActivity.this, "" + message, Toast.LENGTH_SHORT).show();



                    } else {
                        ViewDialog alert = new ViewDialog();
                        alert.showDialog(OtpVerificationActivity.this, message);
                        //Toast.makeText(MainActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                    }

                    GlobalVariables.progressDialog.dismiss();

                }
            });


        } catch (Exception ex) {
            //  Log.e("Could not parse malformed JSON: \"" + json + "\"");
            try {
                resend_otp_response = new JSONObject();
                resend_otp_response.put("result", "failed");
                resend_otp_response.put("data", str);
                resend_otp_response.put("error", ex.getMessage());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        // tv_response.setText("ID:"+id+" and Name:"+name);
                        Toast.makeText(OtpVerificationActivity.this, "" + ex.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
            } catch (Exception exx) {
            }
            GlobalVariables.progressDialog.dismiss();
        }

        return resend_otp_response;
    }

    private void ResendOTPTimer() {
        new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                tv_resend_otp_timer.setText(" 00:" + millisUntilFinished / 1000);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                tv_resend_otp.setEnabled(true);
                tv_resend_otp.setAlpha(1f);
            }

        }.start();
    }
    private void checkConnection() {

        // initialize intent filter
        IntentFilter intentFilter = new IntentFilter();

        // add action
        intentFilter.addAction("android.new.conn.CONNECTIVITY_CHANGE");

        // register receiver
        registerReceiver(new ConnectionReceiver(), intentFilter);

        // Initialize listener
        ConnectionReceiver.Listener = this;

        // Initialize connectivity manager
        ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        // Initialize network info
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

        // get connection status
        isConnected = networkInfo != null && networkInfo.isConnectedOrConnecting();


    }

    @Override
    public void onNetworkChange(boolean isConnected) {

    }
}