package com.sps.shuttelserviceapp.NotificationService;

import static android.content.Context.MODE_PRIVATE;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.Set;

public class SharedPreferencesManager {
    private static SharedPreferences sharedPref;
    private SharedPreferencesManager() { }

    public static void init(Context context) {
        if(sharedPref == null) {
          //  SharedPreferences sh = _context.getSharedPreferences("MySharedPref", MODE_PRIVATE);
//
            sharedPref = context.getSharedPreferences("Notification", MODE_PRIVATE);
        }
    }

    public static final String FCM_DEVICE_TOKEN = "SPS_FCM_DEVICE_TOKEN";
    @SuppressLint("SuspiciousIndentation")
    public static @Nullable
    String readDeviceToken() {
        try {
            if(sharedPref != null)
              //  init(this);
            return sharedPref.getString(FCM_DEVICE_TOKEN, "DEFAULT");
            Log.i("Shared Pref ","Null");
        }catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }
    public static void writeDeviceToken(String value) {

        Log.i("Token", "Added Token Succesfully in shared");
        if(sharedPref!= null) {
            SharedPreferences.Editor prefsEditor = sharedPref.edit();
            prefsEditor.putString(FCM_DEVICE_TOKEN, value);
            prefsEditor.apply();
            Log.i("Notification Token", "Added Successfully " + value);
        }else{
            Log.i("Shared Pref ","Null");
        }
    }

    public static final String REGISTER_TOKEN = "REG_DEVICE_TOKEN";
    public static @Nullable
    String regDeviceToken() {
        try {
            if(sharedPref != null)
                //  init(this);
                return sharedPref.getString(REGISTER_TOKEN, "NO");
            Log.i("Shared Pref ","Null");
        }catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }
    public static void setRegDeviceToken(String value) {

        Log.i("Token", "Added Token Succesfully in shared");
        if(sharedPref!= null) {
            SharedPreferences.Editor prefsEditor = sharedPref.edit();
            prefsEditor.putString(REGISTER_TOKEN, value);
            prefsEditor.apply();
            Log.i("Notification Token", "Added Succesfully " + value);
        }
        else{
            Log.i("Shar Reg Device Token ","Null");
        }
    }

    public static final String FCM_CHANNEL_LIST = "SPS_FCM_CHANNEL_LIST";
    public static @Nullable String[] readChannelList() {
        return (String[]) sharedPref.getStringSet(FCM_CHANNEL_LIST, null).toArray();
    }
    public static void writeChannelList(Set<String> value) {
        SharedPreferences.Editor prefsEditor = sharedPref.edit();
        prefsEditor.putStringSet(FCM_CHANNEL_LIST, value);
        prefsEditor.apply();
    }
}


