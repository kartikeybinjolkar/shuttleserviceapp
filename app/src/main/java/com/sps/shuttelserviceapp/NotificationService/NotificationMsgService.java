package com.sps.shuttelserviceapp.NotificationService;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.sps.shuttelserviceapp.Utils.GlobalVariables;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class NotificationMsgService {

    String tokenN = "";
    JSONObject response;
    String url,json_data;

    public void updatePushNotification(String userMobileid,String tokenNotif){

    }

    public void returnToken(String mobileNo) {
        FirebaseMessaging.getInstance().getToken().addOnCompleteListener(new OnCompleteListener<String>() {
            @Override
            public void onComplete(@NonNull Task<String> task) {
                if (!task.isSuccessful()) {
                    Log.e("In Main", "Token Failed", task.getException());
                    return;
                }
                tokenN = task.getResult();
                Log.e("Notification","In Return Token");
                checkToken(tokenN,mobileNo);
                Log.e("In Return Token ", tokenN);
            }
        });
       // return tokenN;
    }

    public void checkToken(String token,String mobileNo){
        try {
            if(token != null) {
               String oldToken = SharedPreferencesManager.readDeviceToken();
               String regToken = SharedPreferencesManager.regDeviceToken();
               Log.e("Notification","Registered Already" + regToken);
               if(regToken != null && regToken.equals("YES")) {
                   if (oldToken != null && token.equals(oldToken)) {
                       return;
                   }
               }
                SharedPreferencesManager.writeDeviceToken(token);
                SharedPreferencesManager.setRegDeviceToken("YES");
                setNotificationTokenService(token,mobileNo);
            }
        }catch(Exception e){
            Log.e("Notification","error" + e);
            e.printStackTrace();
        }
    }

    public void setNotificationTokenService(String token,String mobileNo){

        url = GlobalVariables.API_URL + "notification/firebase-token";

        json_data = "{\"token\": \"" + token + "\",\n  " + " \"mobile\": \"" + mobileNo + "\"\n}";

        Log.e("Notification URL = >", url);
        Log.e("Notification Json = >", json_data);
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    getNotificationAPI(url, json_data);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    public JSONObject getNotificationAPI(String url, String json) {

        JSONObject jsonObjectResp = null;
        try {

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            OkHttpClient client = new OkHttpClient();
            RequestBody body = RequestBody.create(JSON, json);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    //.post(body)
                    .method("POST", body)
                    //.addHeader(Header_id, Header_value)
                    .build();

            Log.e("Notification","Response got");
            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {

                Log.e("Response Notification", networkResp);
                handleNotificationResponse(networkResp);
            }else{
                SharedPreferencesManager.setRegDeviceToken("NO");
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            Log.e("error",err);
            ex.printStackTrace();
            SharedPreferencesManager.setRegDeviceToken("NO");
        }

        return jsonObjectResp;
    }

    public void handleNotificationResponse(final String str) {
        //dialogshow.dismiss();
        String id , name;
        response = null;
        try {
            response = new JSONObject(str);
            String message = response.getString("msg");
            int code = response.getInt("code");
            Log.e("Code " , code + "");

            if (code == 21) {
                try {
                    SharedPreferencesManager.setRegDeviceToken("YES");
                    Log.e("Notification " , "Saved");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                SharedPreferencesManager.setRegDeviceToken("NO");
            }

        } catch (JSONException e) {
            e.printStackTrace();
            SharedPreferencesManager.setRegDeviceToken("NO");
        }
    }
}
