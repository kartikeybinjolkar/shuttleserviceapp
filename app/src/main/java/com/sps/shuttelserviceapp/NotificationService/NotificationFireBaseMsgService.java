package com.sps.shuttelserviceapp.NotificationService;


import android.app.NotificationManager;
import android.content.Context;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.sps.shuttelserviceapp.R;

public class NotificationFireBaseMsgService extends FirebaseMessagingService {
    private NotificationManager myNotificationManager;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage){
        super.onMessageReceived(remoteMessage);
        Log.d("MessageService","Meesagereceived");
        Log.e("Remote Notification", String.valueOf(remoteMessage.getNotification()));
        Log.e("Remote From", remoteMessage.getFrom());
        Log.e("Remote From", String.valueOf(remoteMessage.getData()));


        if(remoteMessage.getNotification() != null) {
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this, "Notify");
            Log.e("Notification Title",remoteMessage.getNotification().getTitle());
           // Log.e("Notification Body",remoteMessage.getNotification().getBody());
            if(remoteMessage.getNotification().getTitle() != null) {
                mBuilder.setContentTitle(remoteMessage.getNotification().getTitle() );
            }
            if(remoteMessage.getNotification().getBody() != null) {
                mBuilder.setContentText(remoteMessage.getNotification().getBody());
            }
            //  mBuilder.setTicker("Explicit: New Message Received!");
            mBuilder.setSmallIcon(R.drawable.app_icon);
            mBuilder.setAutoCancel(true);

                myNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            // pass the Notification object to the system
            myNotificationManager.notify(123, mBuilder.build());
        }
    }

    @Override
    public void onNewToken(String token) {
        try {
            String oldToken = SharedPreferencesManager.readDeviceToken();
            if (oldToken != null && token.equals(oldToken)) {
                return;
            }
            SharedPreferencesManager.writeDeviceToken(token);

        }catch(Exception e){
            e.printStackTrace();
        }
    }


}