package com.sps.shuttelserviceapp.Controllers;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sps.shuttelserviceapp.HistoryOrderDetailsActivity;
import com.sps.shuttelserviceapp.Models.HistoryModel;
import com.sps.shuttelserviceapp.R;
import com.sps.shuttelserviceapp.Utils.GlobalVariables;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.TimeZone;

public class HistoryListController extends RecyclerView.Adapter<HistoryListController.SingleViewHolder>{

    private Context context;
    private ArrayList<HistoryModel> historyModels;
    int pos;
    private int checkedPosition = 0;
    private int selectedItem;
    public HistoryListController(Context context, ArrayList<HistoryModel> historyModels) {
        this.context = context;
        this.historyModels = historyModels;

    }

    public void setHistoryListDetails(ArrayList<HistoryModel> historyModels) {
        this.historyModels = new ArrayList<>();
        this.historyModels = historyModels;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SingleViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.history_list_details, viewGroup, false);
        return new SingleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SingleViewHolder singleViewHolder,@SuppressLint("RecyclerView") int position) {
        singleViewHolder.bind(historyModels.get(position));

        singleViewHolder.ll_history_card.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onClick(View view) {
                // imageView.setVisibility(View.VISIBLE);
                pos = singleViewHolder.getAdapterPosition() ;
                HistoryModel historyModel = historyModels.get(pos);
                GlobalVariables.history_order_item_id = historyModel.getHistory_item_id();
                /*singleViewHolder.itemView.setBackgroundColor(position == position ? R.color.dark_grey : Color.TRANSPARENT);*/
//                GlobalVariables.selected_enforcement_type = historyModel.getHistory_item_name();
//                Log.e("enf type=>",GlobalVariables.selected_enforcement_type);
//                GlobalVariables.selected_enforcement_amount = historyModel.getHistory_item_amount();
//                GlobalVariables.selected_enforcement_status = historyModel.getHistory_item_status();
//                GlobalVariables.selected_enforcement_number_plate = historyModel.getVehicle_number_plate();
//                GlobalVariables.selected_enforcement_created_at = historyModel.getEnforcement_created_at();
//                GlobalVariables.selected_enforcement_updated_at = historyModel.getEnforcement_updated_at();
//               // GlobalVariables.selected_enforcement_name = historyModels.getEnforcement_name();
                Intent intent= new Intent(context, HistoryOrderDetailsActivity.class);
                context.startActivity(intent);

//                }
            }
        });
    }



    @Override
    public int getItemCount() {
        return historyModels.size();
    }



    class SingleViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_history_id,tv_history_service_name,tv_start_date,tv_end_date;
        private LinearLayout ll_history_card;
        private ImageView iv_vas_image;
        private TextView history_date;

        SingleViewHolder(@NonNull View itemView) {
            super(itemView);

            tv_history_id = (TextView)itemView.findViewById(R.id.tv_history_id);
            tv_history_service_name = (TextView)itemView.findViewById(R.id.tv_history_service_name);
            tv_start_date  = (TextView)itemView.findViewById(R.id.tv_start_date);
            tv_end_date = (TextView)itemView.findViewById(R.id.tv_end_date);

            iv_vas_image = (ImageView) itemView.findViewById(R.id.iv_vas_image);
            history_date = (TextView) itemView.findViewById(R.id.history_date_tv);






            ll_history_card = (LinearLayout) itemView.findViewById(R.id.ll_history_card);

            //imageView = itemView.findViewById(R.id.imageView);
        }

        void bind(final HistoryModel historyModel) {
            if (checkedPosition == -1) {
                // imageView.setVisibility(View.GONE);
            } else {
                if (checkedPosition == getAdapterPosition()) {
                    //   imageView.setVisibility(View.VISIBLE);
                } else {
                    //   imageView.setVisibility(View.GONE);
                }
            }

            if (historyModel.getHistory_date().equals("")){
                history_date.setVisibility(View.GONE);
            }else{
                history_date.setVisibility(View.VISIBLE);
                history_date.setText(historyModel.getHistory_date());
            }
            tv_history_id.setText("#" + historyModel.getHistory_item_id());
            tv_history_service_name.setText(historyModel.getHistory_item_service_name());
            if (historyModel.getHistory_item_start_date().equals("")||historyModel.getHistory_item_start_date().equals("null"))
            {
                tv_start_date.setText("----");
            }else {
                SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                input.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
                SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy hh:mm aa");

                Date date_time = null;
                try {
                    date_time = input.parse(historyModel.getHistory_item_start_date());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String format_date_time = output.format(date_time);

                tv_start_date.setText(format_date_time);
            }
            if (historyModel.getHistory_item_end_date().equals("")||historyModel.getHistory_item_end_date().equals("null"))
            {
                tv_end_date.setText("----");
            }else {
                SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                input.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
                SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy hh:mm aa");

                Date date_time = null;
                try {
                    date_time = input.parse(historyModel.getHistory_item_end_date());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String format_date_time = output.format(date_time);

                tv_end_date.setText(format_date_time);
            }


            iv_vas_image.setImageDrawable(context.getResources().getDrawable(R.drawable.shuttleimage));

            if (historyModel.getHistory_vas_id().equals("24")){

                iv_vas_image.setImageDrawable(context.getResources().getDrawable(R.drawable.car_wash));
            }
            if (historyModel.getHistory_vas_id().equals("28")){

                iv_vas_image.setImageDrawable(context.getResources().getDrawable(R.drawable.valet_parking));
            }


        }

    }

    public void removeAt(int position) {
        historyModels.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, historyModels.size());
    }
}
