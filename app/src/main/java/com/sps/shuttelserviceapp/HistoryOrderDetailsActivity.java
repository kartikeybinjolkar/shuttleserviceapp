package com.sps.shuttelserviceapp;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sps.shuttelserviceapp.Utils.GlobalVariables;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class HistoryOrderDetailsActivity extends AppCompatActivity {

    JSONObject HistoryItemDetailsResponse;
    TextView tv_order_id,tv_site_name,tv_service,tv_service_sub_type,tv_status,tv_payment_status,tv_no_of_passenger,tv_amount,tv_customer_name,tv_contact_number,tv_order_time,tv_complete_time;
    TextView tv_worker_name,tv_worker_number;
    String full_name;
    ImageView iv_close;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_order_details);

        tv_order_id=findViewById(R.id.tv_order_id);
        tv_site_name=findViewById(R.id.tv_site_name);
        tv_service=findViewById(R.id.tv_service_type);
        tv_service_sub_type=findViewById(R.id.tv_service_sub_type);
        tv_status=findViewById(R.id.tv_status);
        tv_payment_status=findViewById(R.id.tv_payment_status);
        tv_no_of_passenger=findViewById(R.id.tv_no_of_passenger);
        tv_amount=findViewById(R.id.tv_amount);
        tv_customer_name=findViewById(R.id.tv_customer_name);
        tv_contact_number=findViewById(R.id.tv_contact_number);
        tv_order_time=findViewById(R.id.tv_order_time);
        tv_complete_time=findViewById(R.id.tv_complete_time);


        tv_service_sub_type.setText("----");

        iv_close = findViewById(R.id.iv_close);
        iv_close.setOnClickListener(v-> finish());


        String url = GlobalVariables.API_URL +"supervisor/order/"+ GlobalVariables.history_order_item_id;

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    GetHistoryItemDetails(url,"");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }

    public JSONObject GetHistoryItemDetails(String url, String json) {
        String Header_id,Header_value;
        Header_id = "Authorization";
        Header_value = "Bearer "+GlobalVariables.user_token;
        Log.e("testing",url);
        JSONObject jsonObjectResp = null;
        try {
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, json);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .method("GET",null)
                    .addHeader(Header_id, Header_value)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()){
                jsonObjectResp = GetHistoryItemDetailsResponse(networkResp);
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            jsonObjectResp = GetHistoryItemDetailsResponse(err);
        }
        return jsonObjectResp;
    }

    private JSONObject GetHistoryItemDetailsResponse(final String str) {
        HistoryItemDetailsResponse = null;
        try {
            HistoryItemDetailsResponse = new JSONObject(str);
            Log.e("Worker Item Response", String.valueOf(HistoryItemDetailsResponse));

            String message = HistoryItemDetailsResponse.getString("msg");
            int code = HistoryItemDetailsResponse.getInt("code");

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (code == 20){
                        try {
                            JSONObject json = new JSONObject(str);
                            JSONObject jsonObject = json.getJSONObject("data");
                            String order_id = jsonObject.getString("id");
                            tv_order_id.setText(order_id);

                            String no_of_passenger = jsonObject.getString("qty");
                            tv_no_of_passenger.setText(no_of_passenger);

                            String amount = jsonObject.getString("amount");
                            tv_amount.setText(amount + " SAR");

                            String order_time = jsonObject.getString("start_time");

                            if (order_time.equals("")||order_time.equals("null"))
                            {
                                tv_order_time.setText("----");
                            }else {
                                SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                                input.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
                                SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy hh:mm aa");

                                Date date_time = null;
                                try {
                                    date_time = input.parse(order_time);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                String format_date_time = output.format(date_time);

                                tv_order_time.setText(format_date_time);
                            }

                            String complete_time = jsonObject.getString("end_time");


                            if (complete_time.equals("")||complete_time.equals("null"))
                            {
                                tv_complete_time.setText("----");
                            }else {
                                SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                                input.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
                                SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy hh:mm aa");

                                Date date_time = null;
                                try {
                                    date_time = input.parse(complete_time);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                String format_date_time = output.format(date_time);

                                tv_complete_time.setText(format_date_time);
                            }

                            JSONObject sites = jsonObject.getJSONObject("sites");
                            String site_name = sites.getString("name");
                            tv_site_name.setText(site_name);

                            JSONObject vas_types = jsonObject.getJSONObject("vas_types");
                            String service = vas_types.getString("name");
                            tv_service.setText(service);

                            JSONObject vas_sub_types = jsonObject.getJSONObject("vas_sub_types");
                            String service_sub_type = vas_sub_types.getString("name");
                            tv_service_sub_type.setText(service_sub_type);

                            String qty = jsonObject.getString("qty");
                            tv_no_of_passenger.setText(qty);

                            JSONObject vas_order_status_types = jsonObject.getJSONObject("vas_order_status_types");
                            String status = vas_order_status_types.getString("name");
                            tv_status.setText(status);

                            if (jsonObject.has("reservations") && !jsonObject.isNull("reservations")) {

                                JSONObject reservations = jsonObject.getJSONObject("reservations");
                                if (reservations.has("customers") && !reservations.isNull("customers")) {
                                    Log.e("Checking", ("Customer"));
                                    JSONObject customers = reservations.getJSONObject("customers");
                                    String first_name = customers.getString("full_name");
                                    String mobile = customers.getString("mobile");
                                    if (customers.has("country_code") && !customers.isNull("country_code")){
                                        JSONObject country_code_obj = customers.getJSONObject("country_code");
                                        String dial_code = country_code_obj.getString("dial_code");
                                        tv_customer_name.setText(first_name);
                                        tv_contact_number.setText(dial_code + " " +mobile);
                                    }else{
                                        tv_customer_name.setText(first_name);
                                        tv_contact_number.setText("+966 " + mobile);
                                    }
                                }else{
                                    Log.e("Checking", "Guest");
                                    JSONObject guest_users = reservations.getJSONObject("guest_users");
                                    String first_name = guest_users.getString("full_name");
                                    String last_name = guest_users.getString("last_name");
                                    if(last_name.equals("") || last_name.equals("null")){
                                        full_name = first_name;
                                    } else {
                                        full_name = first_name + " " + last_name;
                                    }
                                    String mobile = guest_users.getString("mobile");
                                    if (guest_users.has("country_code") && !guest_users.isNull("country_code")){
                                        JSONObject country_code_obj = guest_users.getJSONObject("country_code");
                                        String dial_code = country_code_obj.getString("dial_code");
                                        tv_customer_name.setText(full_name);
                                        tv_contact_number.setText(dial_code + " " +mobile);
                                    }else{
                                        tv_customer_name.setText(full_name);
                                        tv_contact_number.setText("+966 " + mobile);
                                    }
                                }
                                JSONObject payment = reservations.getJSONObject("payment_status");
                                String payment_status = payment.getString("name");
                                tv_payment_status.setText(payment_status);



                            }else{
                                tv_customer_name.setText("");
                                tv_contact_number.setText("");
                            }




                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (code == 43) {
                        SharedPreferences prefs = getSharedPreferences(
                                "saved_data", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("login_status", "");
                        editor.putString("token", "");
                        editor.putString("role_id", "");
                        editor.putString("user_name", "");
                        editor.apply();
                        Toast.makeText(HistoryOrderDetailsActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(HistoryOrderDetailsActivity.this, LoadingActivity.class));

                    }
                }
            });

        } catch (JSONException ex) {
            try {
                HistoryItemDetailsResponse = new JSONObject();
                HistoryItemDetailsResponse.put("result","failed");
                HistoryItemDetailsResponse.put("data",str);
                HistoryItemDetailsResponse.put("error",ex.getMessage());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(HistoryOrderDetailsActivity.this,""+ex.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (Exception exx) {
            }
        }
        return HistoryItemDetailsResponse;
    }
}