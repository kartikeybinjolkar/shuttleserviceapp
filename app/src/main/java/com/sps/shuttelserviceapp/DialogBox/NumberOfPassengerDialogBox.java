package com.sps.shuttelserviceapp.DialogBox;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sps.shuttelserviceapp.R;
import com.sps.shuttelserviceapp.Utils.GlobalVariables;
import com.sps.shuttelserviceapp.Utils.ViewDialog;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class NumberOfPassengerDialogBox extends AppCompatActivity {

    Activity mActivity;
    Dialog dialog;
    Button btn_confirm;
    Button ig_increment,ig_decrement;
    int count = 0;
    TextView tv_display_passenger;
    JSONObject confirm_button_response;


    public void showDialog(Activity activity){
        mActivity = activity;
        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);


        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
        wmlp.gravity = Gravity.BOTTOM | Gravity.LEFT;
        wmlp.y = 10;
        dialog.getWindow().setGravity(Gravity.CENTER);
        dialog.show();
        dialog.setContentView(R.layout.activity_number_of_passenger_dialog_box);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        tv_display_passenger = dialog.findViewById(R.id.tv_display_passenger);
        ig_increment = dialog.findViewById(R.id.btn_inc);
        ig_decrement = dialog.findViewById(R.id.btn_dec);
        btn_confirm = dialog.findViewById(R.id.btn_confirm);

       ig_increment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count ++;
                tv_display_passenger.setText(""+count);
            }
        });

        ig_decrement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count<=0){
                    count=0;
                }
                else{
                    count --;
                    tv_display_passenger.setText(""+count);
                }
            }
        });

        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()){
                    case R.id.btn_confirm:
                        dialog.dismiss();
                        Log.e("Confirm button",String.valueOf(btn_confirm));
                        String url = GlobalVariables.API_URL + "worker/status/" + GlobalVariables.order_id;
                        String input_json = "{\n" +
                                "  \"status\": \""+"done"+"\",\n" +
                                "  \"name\": \""+tv_display_passenger+"\",\n" +
                                "}";

                        Thread thread = new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try{
                                    GetDialogConfirmButton(url,input_json);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                        thread.start();

                }
            }
        });
    }

    public JSONObject GetDialogConfirmButton(String url, String json) {
        String Header_id,Header_value;
        Header_id = "Authorization";
        Header_value = "Baerer "+ GlobalVariables.user_token;
        Log.e("Change Status URL",url);
        JSONObject jsonObjectResp = null;

        try {

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, json);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .method("PATCH",body)
                    .addHeader(Header_id, Header_value)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                jsonObjectResp = GetDialogConfirmButtonResponse(networkResp);
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            jsonObjectResp = GetDialogConfirmButtonResponse(err);
        }

        return jsonObjectResp;
    }

    private JSONObject GetDialogConfirmButtonResponse(final String str) {
        confirm_button_response=null;
        try {
            confirm_button_response = new JSONObject(str);
            String message = confirm_button_response.getString("msg");
            int code = confirm_button_response.getInt("code");

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if(code == 20){
                    }
                    else {
                        ViewDialog alert = new ViewDialog();
                        alert.showDialog(NumberOfPassengerDialogBox.this, message);
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return confirm_button_response;
    }
}
