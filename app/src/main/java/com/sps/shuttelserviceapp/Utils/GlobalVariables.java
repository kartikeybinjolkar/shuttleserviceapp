package com.sps.shuttelserviceapp.Utils;

import static androidx.core.content.ContextCompat.getSystemService;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.view.WindowManager;

import com.sps.shuttelserviceapp.R;


public class GlobalVariables {


    //Demo URLs//
   public static String API_URL  = "https://sp.demoapi.v2.hyperthings.in/api/v1/";
    public static String QR_Code_URL  = "https://demo.cportal.hyperthings.in/";

    //Staging URLs
//    public static String API_URL  = "https://iot.hyperthink.io:7006/api/v1/";;
//    public static String QR_Code_URL  = "https://iot.hyperthink.io:7004/";

    //Production URLs
//    public static String API_URL  = "https://apis.mawqfi.com/api/v1/";
//    public static String QR_Code_URL  = "https://mawqfi.com/";

    public static String android_id="";

    public static String phone_number="";
    public static String country_code="";

    public static String user_name = "";
    public static String user_role_id="";
    public static String user_token="";

    public static String ticket_number = "";

    public static String assigned_works = "";
    public static String pending_works = "";

    public static String order_id ="";
    public static String pass_id = "";
    public static String order_status_id ="";
    public static String order_item_id="";

    public static String order_list_id="";

    public static String Clicked_image_url="";

    public static String Passenger_count="";

    public static String history_order_item_id="";
    public static String worker_id="";
    public static String worker_uid="";
    public static String driver_id="";

    public static String after_ticket_generated_site_name="";
    public static String after_ticket_generated_gate="";
    public static String after_ticket_generated_ticket_id="";
    public static String after_ticket_generated_datetime="";
    public static String after_ticket_generated_vehicle_no="";
    public static String reservation_type_id="";
    public static String selected_gate_name="";
    public static int vas_type_id = 3;

    public static ProgressDialog progressDialog;
    public static String siteid = "";


    public static ProgressDialog createProgressDialog(Context context) {
        ProgressDialog dialog = new ProgressDialog(context);
        try {
            dialog.show();
        } catch (WindowManager.BadTokenException e) {

        }
        dialog.setCancelable(false);
        dialog.getWindow()
                .setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setContentView(R.layout.progressdialog);
        // dialog.setMessage(Message);
        return dialog;
    }



}
