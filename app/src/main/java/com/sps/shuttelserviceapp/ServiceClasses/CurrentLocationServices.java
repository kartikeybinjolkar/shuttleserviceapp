package com.sps.shuttelserviceapp.ServiceClasses;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import com.sps.shuttelserviceapp.Utils.GlobalVariables;

import org.json.JSONObject;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class CurrentLocationServices extends Service implements LocationListener {
    boolean isGPSEnable = false;
    boolean isNetworkEnable = false;
    double latitude, longitude;
    LocationManager locationManager;
    Location location;
    private Handler mHandler = new Handler();
    private Timer mTimer = null;
    long notify_interval = 30000;
    public static String str_receiver = "com.sps.shuttelserviceapp.Services.receiver";
    Intent intent;
    public CurrentLocationServices() {
    }
    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }
    @Override
    public void onCreate() {
        super.onCreate();
        intent = new Intent(str_receiver);//
        fn_getlocation();
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("TAG", "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        mTimer = new Timer();
        mTimer.schedule(new TimerTaskToGetLocation(), 5, notify_interval);
        return START_STICKY;
    }
    @Override
    public void onLocationChanged(@NonNull Location location) {
    }
    @Override
    public void onLocationChanged(@NonNull List<Location> locations) {
        LocationListener.super.onLocationChanged(locations);
    }
    @Override
    public void onFlushComplete(int requestCode) {
        LocationListener.super.onFlushComplete(requestCode);
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        LocationListener.super.onStatusChanged(provider, status, extras);
    }
    @Override
    public void onProviderEnabled(@NonNull String provider) {
        LocationListener.super.onProviderEnabled(provider);
    }
    @Override
    public void onProviderDisabled(@NonNull String provider) {
        LocationListener.super.onProviderDisabled(provider);
    }
    private void fn_getlocation() {
        locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        isGPSEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!isGPSEnable && !isNetworkEnable) {

        } else {

            if (isNetworkEnable) {
                location = null;
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
// TODO: Consider calling
// ActivityCompat#requestPermissions
// here to request the missing permissions, and then overriding
// public void onRequestPermissionsResult(int requestCode, String[] permissions,
// int[] grantResults)
// to handle the case where the user grants the permission. See the documentation
// for ActivityCompat#requestPermissions for more details.
                    return;
                }
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);
                if (locationManager != null) {
                    location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (location!=null){

                    }else{
                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    }
                    if (location != null) {

                        Log.e("latitude", location.getLatitude() + "");
                        Log.e("longitude", location.getLongitude() + "");

                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                        fn_update(location);
                    }
                }

            }

        }

    }
    private class TimerTaskToGetLocation extends TimerTask {
        @Override
        public void run() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    fn_getlocation();
                }
            });
        }
    }
    private void fn_update(Location location){
        intent.putExtra("latutide",location.getLatitude()+"");
        intent.putExtra("longitude",location.getLongitude()+"");
        sendBroadcast(intent);
        String url = GlobalVariables.API_URL + "worker/tracking";
        String send_input_json = "{\n" +
                "  \"device_id\": \""+ GlobalVariables.android_id+"\",\n" +
                "  \"lat\": \""+location.getLatitude()+"\",\n" +
                "  \"long\": \""+location.getLongitude()+"\"\n" +
                "}";
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    GenerateRequest(url, send_input_json);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        thread.start();
    }
    public JSONObject GenerateRequest(String url, String json) {
        Log.e("send_url=>",url);
        Log.e("send_json=>",json);
        String Header_id,Header_value;
        Header_id = "Authorization";
        Header_value = "Bearer " + GlobalVariables.user_token;
        Log.e("Headervalue -------",GlobalVariables.user_token);
        JSONObject jsonObjectResp = null;
        try {
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");
            OkHttpClient client = new OkHttpClient();
            RequestBody body = RequestBody.create(JSON, json);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .method("POST",body)
                    .addHeader(Header_id, Header_value)
                    .build();
            okhttp3.Response response = client.newCall(request).execute();
            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                jsonObjectResp = GenerateResponse(networkResp);
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            jsonObjectResp = GenerateResponse(err);
        }
        return jsonObjectResp;
    }
    public JSONObject GenerateResponse(final String str) {
//dialogshow.dismiss();
        String id, name;
        JSONObject send_otp_response = null;
        try {
            send_otp_response = new JSONObject(str);
            Log.e("Current location response=>", String.valueOf(send_otp_response));
            String message = send_otp_response.getString("msg");
            int code = send_otp_response.getInt("code");
        } catch (Exception ex) {
//  Log.e("Could not parse malformed JSON: \"" + json + "\"");
            try {
                send_otp_response = new JSONObject();
                send_otp_response.put("result", "failed");
                send_otp_response.put("data", str);
                send_otp_response.put("error", ex.getMessage());
            } catch (Exception exx) {
            }
            GlobalVariables.progressDialog.dismiss();
        }
        return send_otp_response;
    }
}

