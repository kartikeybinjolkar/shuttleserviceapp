package com.sps.shuttelserviceapp.Models;

public class WorkerHoursModel {

    private int days;
    private String first_half_start;
    private String first_half_end;
    private String second_half_start;
    private String second_half_end;


    public String getFirst_half_start() {
        return first_half_start;
    }

    public void setFirst_half_start(String first_half_start) {
        this.first_half_start = first_half_start;
    }

    public String getFirst_half_end() {
        return first_half_end;
    }

    public void setFirst_half_end(String first_half_end) {
        this.first_half_end = first_half_end;
    }

    public String getSecond_half_start() {
        return second_half_start;
    }

    public void setSecond_half_start(String second_half_start) {
        this.second_half_start = second_half_start;
    }

    public String getSecond_half_end() {
        return second_half_end;
    }

    public void setSecond_half_end(String second_half_end) {
        this.second_half_end = second_half_end;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }
}
