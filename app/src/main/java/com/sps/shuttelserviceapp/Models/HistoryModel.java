package com.sps.shuttelserviceapp.Models;

public class HistoryModel {
    private String history_date;
    private String history_item_id;
    private String history_item_service_name;
    private String history_item_start_date;
    private String history_item_end_date;
    private String history_vas_id;

    public String getHistory_item_id() {
        return history_item_id;
    }

    public void setHistory_item_id(String history_item_id) {
        this.history_item_id = history_item_id;
    }


    public String getHistory_item_service_name() {
        return history_item_service_name;
    }

    public void setHistory_item_service_name(String history_item_service_name) {
        this.history_item_service_name = history_item_service_name;
    }

    public String getHistory_item_start_date() {
        return history_item_start_date;
    }

    public void setHistory_item_start_date(String history_item_start_date) {
        this.history_item_start_date = history_item_start_date;
    }

    public String getHistory_item_end_date() {
        return history_item_end_date;
    }

    public void setHistory_item_end_date(String history_item_end_date) {
        this.history_item_end_date = history_item_end_date;
    }


    public String getHistory_vas_id() {
        return history_vas_id;
    }

    public void setHistory_vas_id(String history_vas_id) {
        this.history_vas_id = history_vas_id;
    }

    public String getHistory_date() {
        return history_date;
    }

    public void setHistory_date(String history_date) {
        this.history_date = history_date;
    }
}
