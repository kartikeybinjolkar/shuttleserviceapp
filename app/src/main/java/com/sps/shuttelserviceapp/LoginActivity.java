package com.sps.shuttelserviceapp;

import static android.Manifest.permission.READ_SMS;
import static androidx.core.content.ContextCompat.getSystemService;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;
import com.sps.shuttelserviceapp.InternetCheck.ConnectionReceiver;
import com.sps.shuttelserviceapp.Utils.GlobalVariables;
import com.sps.shuttelserviceapp.Utils.ViewDialog;

import org.json.JSONObject;

import java.net.InetAddress;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener,ConnectionReceiver.ReceiverListener {

    boolean isConnected;
    CountryCodePicker sign_in_ccp;
    String sign_in_selected_country_code="+966";
    EditText et_phone_number;
    String send_otp_input_json;
    Button btn_send_otp;
    JSONObject send_otp_response;
    private static final int REQUEST_PERMISSIONS = 100;
    boolean boolean_permission;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        fn_permission();
        et_phone_number = findViewById(R.id.et_phone_number);
        sign_in_ccp = findViewById(R.id.sign_in_ccp);

        btn_send_otp =findViewById(R.id.btn_send_otp);
        btn_send_otp.setOnClickListener(this);

        sign_in_ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {

                sign_in_selected_country_code = sign_in_ccp.getSelectedCountryCodeWithPlus();

                // Toast.makeText(LoginActivity.this, "Updated " + signup_ccp.getSelectedCountryCodeWithPlus(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void fn_permission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if ((ContextCompat.checkSelfPermission(getApplicationContext(), READ_SMS) != PackageManager.PERMISSION_GRANTED)) {

                if ((ActivityCompat.shouldShowRequestPermissionRationale(LoginActivity.this, READ_SMS))) {


                } else {
                    ActivityCompat.requestPermissions(LoginActivity.this, new String[]{READ_SMS}, REQUEST_PERMISSIONS);
                }
            } else {
                boolean_permission = true;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_PERMISSIONS: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    boolean_permission = true;
                } else {
                    Toast.makeText(getApplicationContext(), "Please allow the permission", Toast.LENGTH_LONG).show();

                }
            }
        }
    }

    public void onBackPressed() {
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_send_otp:

                if(et_phone_number.getText().toString().equals("")){
                    ViewDialog alert = new ViewDialog();
                    alert.showDialog(LoginActivity.this, "Please Enter Mobile Number");
                }else if(et_phone_number.getText().toString().length()<8){
                    ViewDialog alert = new ViewDialog();
                    alert.showDialog(LoginActivity.this, "Minimum 8 numbers Required");
                }
                else{
                    checkConnection();
                    if(isConnected){

                        String url = GlobalVariables.API_URL + "worker/generate-otp";

                        send_otp_input_json = "{\n" +
                                "  \"mobile\": \""+et_phone_number.getText().toString()+"\",\n" +
                                "  \"country_code_id\": \""+sign_in_selected_country_code+"\"\n" +
                                "}";

                        GlobalVariables.progressDialog = GlobalVariables.createProgressDialog(LoginActivity.this);
                        GlobalVariables.progressDialog.show();

                        Thread thread = new Thread(new Runnable() {

                            @Override
                            public void run() {
                                try {
                                    GenerateOTP(url, send_otp_input_json);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        });
                        thread.start();

                    }else{

                        Log.e("Checking 2 ", String.valueOf(isConnected));
                        Toast.makeText(this,"Please Check your Internet Connection !",Toast.LENGTH_LONG).show();

                    }


                }

                break;
        }
    }

    public JSONObject GenerateOTP(String url, String json) {

        Log.e("send_otp_url=>",url);
        Log.e("send_otp_json=>",json);
        String Header_id,Header_value;


        JSONObject jsonObjectResp = null;

        try {

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, json);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .method("POST",body)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                jsonObjectResp = GenerateOTPResponse(networkResp);
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            jsonObjectResp = GenerateOTPResponse(err);
        }

        return jsonObjectResp;
    }

    public JSONObject GenerateOTPResponse(final String str) {

        String id, name;
        send_otp_response = null;
        try {
            send_otp_response = new JSONObject(str);
            Log.e("user profile Response=>", String.valueOf(send_otp_response));

            String message = send_otp_response.getString("msg");
            int code = send_otp_response.getInt("code");


            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (code == 20) {

                        Toast.makeText(LoginActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                        GlobalVariables.phone_number = et_phone_number.getText().toString();
                        GlobalVariables.country_code = sign_in_selected_country_code;
                        Intent otp_verification = new Intent(LoginActivity.this,OtpVerificationActivity.class );
                        startActivity(otp_verification);
                        finish();

                    } else {
                        ViewDialog alert = new ViewDialog();
                        alert.showDialog(LoginActivity.this, message);
                        //Toast.makeText(MainActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                    }

                    GlobalVariables.progressDialog.dismiss();

                }
            });


        } catch (Exception ex) {
            //  Log.e("Could not parse malformed JSON: \"" + json + "\"");
            try {
                send_otp_response = new JSONObject();
                send_otp_response.put("result", "failed");
                send_otp_response.put("data", str);
                send_otp_response.put("error", ex.getMessage());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        // tv_response.setText("ID:"+id+" and Name:"+name);
                        Toast.makeText(LoginActivity.this, "" + ex.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.e("Error msg",ex.getMessage());

                    }
                });
            } catch (Exception exx) {
            }
            GlobalVariables.progressDialog.dismiss();
        }

        return send_otp_response;
    }

    private void checkConnection() {

        // initialize intent filter
        IntentFilter intentFilter = new IntentFilter();

        // add action
        intentFilter.addAction("android.new.conn.CONNECTIVITY_CHANGE");

        // register receiver
        registerReceiver(new ConnectionReceiver(), intentFilter);

        // Initialize listener
        ConnectionReceiver.Listener = this;

        // Initialize connectivity manager
        ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        // Initialize network info
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

        // get connection status
        isConnected = networkInfo != null && networkInfo.isConnectedOrConnecting();


    }

    @Override
    public void onNetworkChange(boolean isConnected) {

    }
}