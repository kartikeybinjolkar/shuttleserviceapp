package com.sps.shuttelserviceapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.widget.Toast;

import com.sps.shuttelserviceapp.ServiceClasses.CurrentLocationServices;
import com.sps.shuttelserviceapp.databinding.ActivityOrderListBinding;

public class OrderListActivity extends AppCompatActivity {

    ActivityOrderListBinding binding;
    private Boolean exit = false;
    boolean boolean_permission;
    private static final int REQUEST_PERMISSIONS = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityOrderListBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        replaceFragment(new HomeFragment());

        fn_permission();
        if (boolean_permission) {
            try {
                /*  if (mPref.getString("service", "").matches(""))
                {
                medit.putString("service", "service").commit();
                Intent intent = new Intent(getApplicationContext(), CurrentLocationService.class);
                startService(intent);
                } else {
                //Toast.makeText(getApplicationContext(), "Service is already running", Toast.LENGTH_SHORT).show();
                }*/
                Intent intent = new Intent(getApplicationContext(), CurrentLocationServices.class);
                startService(intent);
            }catch(Exception e) {
                e.printStackTrace();
            }
        }else {
            Toast.makeText(getApplicationContext(), "Please enable the gps", Toast.LENGTH_SHORT).show();
        }


        binding.bottomNav.setOnItemSelectedListener(item -> {
            switch (item.getItemId()){
                case R.id.home:
                    replaceFragment(new HomeFragment());
                    break;
                case R.id.history:
                    replaceFragment(new HistoryFragment());
                    break;
                case R.id.profile:
                    replaceFragment(new ProfileFragment());
                    break;
            }
            return true;
        });
    }
    private void fn_permission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
        {
            if ((ContextCompat.checkSelfPermission(getApplicationContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED))
            {
                if ((ActivityCompat.shouldShowRequestPermissionRationale(OrderListActivity.this, android.Manifest.permission.ACCESS_FINE_LOCATION)))
                {
                } else {
                    ActivityCompat.requestPermissions(OrderListActivity.this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION
                            },
                            REQUEST_PERMISSIONS);
                }
            } else {
                boolean_permission = true;
            }
        }
    }
    private void replaceFragment(Fragment fragment){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.view,fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed() {
        if (exit) {
            finishAffinity(); // finish activity
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(() -> exit = false, 3 * 1000);
        }

    }
}