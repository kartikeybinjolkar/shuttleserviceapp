package com.sps.shuttelserviceapp;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.sps.shuttelserviceapp.Utils.BluetoothUtil;
import com.sps.shuttelserviceapp.Utils.ESCUtil;
import com.sps.shuttelserviceapp.Utils.GlobalVariables;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class TicketPrintActivity extends AppCompatActivity implements View.OnClickListener{

    Button btn_print,btn_home;
    TextView tv_parent_site_name,print_tv_parent_site_name,tv_site_name,print_tv_site_name,tv_ticket_no,tv_DateTime,tv_vehicle_no;
    ImageView iv_qr_code,print_iv_qr_code;
    TextView  print_tv_ticket_no,print_tv_DateTime,print_tv_vehicle_no,tv_service_type,print_tv_service_type;
    JSONObject get_parent_site_response;
    Handler mHandler = new Handler();
    boolean isHomePressed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket_print);

        tv_parent_site_name = findViewById(R.id.tv_parent_site_name);
        print_tv_parent_site_name = findViewById(R.id.print_tv_parent_site_name);
        tv_site_name = findViewById(R.id.tv_site_name);
        print_tv_site_name = findViewById(R.id.print_tv_site_name);
        tv_ticket_no = findViewById(R.id.tv_ticket_no);
        tv_DateTime = findViewById(R.id.tv_DateTime);
//        tv_vehicle_no = findViewById(R.id.tv_vehicle_no);

        iv_qr_code = findViewById(R.id.iv_qr_code);
        print_iv_qr_code = findViewById(R.id.print_iv_qr_code);


        print_tv_ticket_no = findViewById(R.id.print_tv_ticket_no);
        print_tv_DateTime = findViewById(R.id.print_tv_DateTime);
//        print_tv_vehicle_no = findViewById(R.id.print_tv_vehicle_no);
        print_tv_service_type = findViewById(R.id.print_tv_service_type);
        tv_service_type= findViewById(R.id.tv_service_type);



        tv_site_name.setText(GlobalVariables.after_ticket_generated_site_name);
        print_tv_site_name.setText(GlobalVariables.after_ticket_generated_site_name);
        tv_ticket_no.setText(GlobalVariables.after_ticket_generated_ticket_id);
        print_tv_ticket_no.setText(GlobalVariables.after_ticket_generated_ticket_id);


        if (GlobalVariables.after_ticket_generated_datetime.equals("")||GlobalVariables.after_ticket_generated_datetime.equals(null))
        {
            tv_DateTime.setText("------");
            print_tv_DateTime.setText("------");
        }else {
            SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
            input.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
            SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy hh:mm aa");

            Date date_time = null;
            try {
                date_time = input.parse(GlobalVariables.after_ticket_generated_datetime);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String format_date_time = output.format(date_time);

            tv_DateTime.setText(format_date_time);
            print_tv_DateTime.setText(format_date_time);
        }
//        tv_vehicle_no.setText(GlobalVariables.after_ticket_generated_vehicle_no);
//        print_tv_vehicle_no.setText(GlobalVariables.after_ticket_generated_vehicle_no);



        print_tv_service_type.setText("Service Type: Shuttle Service  ");
        tv_service_type.setText("Service Type: Shuttle Service ");


        btn_print = findViewById(R.id.btn_print);
        btn_print.setOnClickListener(this);

        btn_home = findViewById(R.id.btn_home);
        btn_home.setOnClickListener(this);
        try {
            encodeAsBitmap(GlobalVariables.QR_Code_URL+"ticket/"+GlobalVariables.after_ticket_generated_ticket_id);
        } catch (WriterException e) {
            e.printStackTrace();
        }

        callParentSiteAPI();


    }

    private void callParentSiteAPI() {
        String url = GlobalVariables.API_URL + "sites/parent-site/" + GlobalVariables.siteid;

        GlobalVariables.progressDialog = GlobalVariables.createProgressDialog(this);
        GlobalVariables.progressDialog.show();

        Thread thread = new Thread(() -> {
            try {
                getParentSiteIdRequest(url, "");

            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        thread.start();


    }

    private JSONObject getParentSiteIdRequest(String url, String s) {
        String Header_id, Header_value;
        Header_id = "Authorization";
        Header_value = "Bearer " + GlobalVariables.user_token;
        JSONObject jsonObjectResp = null;

        Log.e("Get Site url=>", url);
        try {

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, s);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .addHeader(Header_id, Header_value)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                jsonObjectResp = getParentSiteIdResponse(networkResp);
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            jsonObjectResp = getParentSiteIdResponse(err);
        }

        return jsonObjectResp;
    }

    private JSONObject getParentSiteIdResponse(String networkResp) {
        get_parent_site_response = null;
        try {
            get_parent_site_response = new JSONObject(networkResp);
            Log.e("Get Amount Resp", String.valueOf(get_parent_site_response));
            String message = get_parent_site_response.getString("msg");
            int code = get_parent_site_response.getInt("code");

            runOnUiThread(() -> {
                if (code == 20){
                    try {
                        JSONObject json = new JSONObject(networkResp);
                        JSONObject data =  json.getJSONObject("data");
                        String id = data.getString("id");
                        String parent_site_name = data.getString("name");
                        boolean is_active = data.getBoolean("is_active");

                        boolean is_parent_site_null = data.isNull("name");

                        if (!is_parent_site_null){
                            tv_parent_site_name.setText(parent_site_name);
                            print_tv_parent_site_name.setText(parent_site_name);

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            });


        }catch (Exception e){
            e.printStackTrace();
        }
        GlobalVariables.progressDialog.dismiss();
        return null;
    }

    Bitmap encodeAsBitmap(String str) throws WriterException {
        QRCodeWriter writer = new QRCodeWriter();
        BitMatrix bitMatrix = writer.encode(str, com.google.zxing.BarcodeFormat.QR_CODE, 400, 400);

        int w = bitMatrix.getWidth();
        int h = bitMatrix.getHeight();
        int[] pixels = new int[w * h];
        for (int y = 0; y < h; y++) {
            for (int x = 0; x < w; x++) {
                pixels[y * w + x] = bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE;
            }
        }
//
        Bitmap bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, w, 0, 0, w, h);
        iv_qr_code.setImageBitmap(bitmap);
        print_iv_qr_code.setImageBitmap(bitmap);
        return bitmap;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btn_print:
                isHomePressed = true;
                View view = findViewById(R.id.print_layout);
                //   printMe.sendViewToPrinter(view);
                printLayout(view);

                break;

            case R.id.btn_home:
                if (isHomePressed){
                    mHandler.removeCallbacksAndMessages(null);
                }
                Intent generate_ticket = new Intent(TicketPrintActivity.this,OrderListActivity.class );
                generate_ticket.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(generate_ticket);
                finish();
                break;
        }
    }

    public void printLayout(View v){
        View view = findViewById(R.id.print_layout);

        try
        {
            if(BluetoothUtil.connectBlueTooth(this))
            {
                Bitmap bitmap2= convertViewToBitmap1(v);

                BluetoothUtil.sendData(ESCUtil.printBitmap( bitmap2));
                BluetoothUtil.sendData(ESCUtil.nextLine(3));

                mHandler.postDelayed(() -> {
                    Intent generate_ticket = new Intent(TicketPrintActivity.this,OrderListActivity.class );
                    generate_ticket.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(generate_ticket);
                    finish();
                },30000);

            } else {
                Toast.makeText(getApplicationContext(), "No able to connect BLE printer found", Toast.LENGTH_SHORT).show();
            }
        }
        catch(Exception ce)
        {
            Toast.makeText(getApplicationContext(), ce.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private Bitmap convertViewToBitmap1(final View mView) {
        @SuppressLint("Range") final int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(ViewGroup.LayoutParams.MATCH_PARENT, View.MeasureSpec.UNSPECIFIED);
        @SuppressLint("Range") final int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(ViewGroup.LayoutParams.WRAP_CONTENT, View.MeasureSpec.UNSPECIFIED);
        mView.measure(widthMeasureSpec, heightMeasureSpec);
        Bitmap b = Bitmap.createBitmap(mView.getMeasuredWidth(), mView.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(b);
        mView.layout(0, 0, mView.getMeasuredWidth(), mView.getMeasuredHeight());
        mView.draw(c);
        return b;
    }

    @Override
    public void onBackPressed() {

    }
}