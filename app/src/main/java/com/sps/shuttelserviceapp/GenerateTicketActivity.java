package com.sps.shuttelserviceapp;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.hbb20.CountryCodePicker;
import com.sps.shuttelserviceapp.Models.WorkerHoursModel;
import com.sps.shuttelserviceapp.Utils.GlobalVariables;
import com.sps.shuttelserviceapp.Utils.ViewDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class GenerateTicketActivity extends AppCompatActivity implements View.OnClickListener{

    Button btn_print_ticket;
    CountryCodePicker generate_ticket_ccp;
    String generate_ticket_selected_country_code = "+966",quantity;
    EditText et_customer_name, et_customer_mobile,et_no_of_passenger;
    TextView tv_valet_service_price, valet_service_value, status_tv,amount_value_tv,filled_exposed_service_sub_type;
    JSONObject sites_name_response;
    ArrayList<String> site_id_array = new ArrayList<String>();
    ArrayList<String> site_name_array = new ArrayList<String>();
    HashMap<String, Object> site_name_hashmap;
    SimpleAdapter site_adapter, type_adapter, vas_adapter;
    ArrayList<HashMap<String, Object>> site_name_list = new ArrayList<>();
    Spinner site_dropdown, service_type_spinner, payment_status_spinner;
    String Selected_SiteName, Selected_SiteId;
    String Selected_vas_type,Selected_vas_id;
    JSONObject get_generate_ticket_details_response;
    HashMap<String, String> vas_sub_type_price = new HashMap<>();
    JSONObject generate_ticket_details_submit_response;
    private ArrayList<WorkerHoursModel> worker_hour_list = new ArrayList<>();
    HashMap<String, Object> type_hashmap;
    SimpleDateFormat input_date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    SimpleDateFormat input_time = new SimpleDateFormat("HH:mm:ss");
    String Selected_TypeName, Selected_TypeId;

    ArrayList<String> vas_type_id_array = new ArrayList<String>();
    ArrayList<String> vas_type_name_array = new ArrayList<String>();

    HashMap<String, Object> vas_hashmap;
    JSONObject vas_type_response;

    Double price;
    JSONObject working_hours_response;

    boolean service_sub_type = false;
    boolean isShuttleService = false;

    ArrayList<HashMap<String, Object>> vas_type_list = new ArrayList<>();

    String submit_add_enforcement_json;
    String cur_date, cur_time, DateTime;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.activity_generate_ticket);
        btn_print_ticket = findViewById(R.id.btn_print_ticket);
        amount_value_tv= findViewById(R.id.amount_value_tv);
        btn_print_ticket.setOnClickListener(this);





        generate_ticket_ccp = findViewById(R.id.generate_ticket_ccp);

        generate_ticket_ccp.setOnCountryChangeListener(() -> {
            generate_ticket_selected_country_code = generate_ticket_ccp.getSelectedCountryCodeWithPlus();
        });

        et_no_of_passenger = findViewById(R.id.no_of_passenger);
        et_customer_name = findViewById(R.id.customer_name_value);
        et_customer_mobile = findViewById(R.id.contact_no_value);

        tv_valet_service_price = findViewById(R.id.unit_cost_value_tv);
        valet_service_value = findViewById(R.id.valet_service_value);
        filled_exposed_service_sub_type = findViewById(R.id.filled_exposed_service_sub_type);
//        valet_service_value.setText("Shuttle Service");
        status_tv = findViewById(R.id.status_value);
//        status_tv.setText("new");
       et_no_of_passenger.setEnabled(false);
        site_dropdown = findViewById(R.id.filled_exposed_site);
      /*  service_type_spinner = findViewById(R.id.filled_exposed_service_sub_type);*/
     //   payment_status_spinner = findViewById(R.id.filled_exposed_payment_status);

        String[] payment_status_data = {"Unpaid", "Paid"};
        String[] service_subtype_data = {"Normal", "Large"};
        et_no_of_passenger.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(et_no_of_passenger.getText().toString().equals("")){

                }else {
                    int passenger_value = Integer.parseInt(et_no_of_passenger.getText().toString());
                    Log.e(String.valueOf(passenger_value),String.valueOf(price));
                    Double amount_value = passenger_value * price ;
                    Log.e("Amount Value ", String.valueOf(amount_value));
                    amount_value_tv.setText("" + amount_value + " SAR");
                }

            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            public void afterTextChanged(Editable s) {

            }
        });


        ArrayAdapter paymentAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, payment_status_data);
       // paymentAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
     //   payment_status_spinner.setAdapter(paymentAdapter);

//        ArrayAdapter service_subtype = new ArrayAdapter(this, android.R.layout.simple_spinner_item, service_subtype_data);
//        service_subtype.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//        service_type_spinner.setAdapter(service_subtype);

        ImageView iv_close = findViewById(R.id.close_iv);

        iv_close.setOnClickListener(v1 -> {
            finish();
        });



        String site_name_url = GlobalVariables.API_URL + "enforcement/get/sites";

        GlobalVariables.progressDialog = GlobalVariables.createProgressDialog(GenerateTicketActivity.this);
        GlobalVariables.progressDialog.show();

        Thread thread = new Thread(() -> {
            try {
                GetSitesNameAPI(site_name_url, "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        thread.start();


        site_dropdown.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        Selected_SiteId = String.valueOf(adapterView.getItemAtPosition(i));
                        site_name_hashmap = (HashMap<String, Object>) site_adapter.getItem(i);
                        Selected_SiteName = (String) site_name_hashmap.get("siteName");
                        Selected_SiteId = (String) site_name_hashmap.get("siteId");
                        Log.e("selected siteName", Selected_SiteName);
                        Log.e("selected siteId", Selected_SiteId);

                        if (Selected_SiteId.equals("")) {

                        } else {
                            String url = GlobalVariables.API_URL + "supervisor/sites/" + Selected_SiteId;
                            GlobalVariables.siteid = Selected_SiteId;

                            GlobalVariables.progressDialog = GlobalVariables.createProgressDialog(GenerateTicketActivity.this);
                            GlobalVariables.progressDialog.show();

                            Thread thread = new Thread(() -> {
                                try {
                                    GetGenerateTicketDetailsAPI(url, "");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            });
                            thread.start();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }

                }
        );

        /*service_type_spinner.setOnItemSelectedListener(
                new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        Selected_vas_id = String.valueOf(parent.getItemAtPosition(position));
                        vas_hashmap = (HashMap<String, Object>) vas_adapter.getItem(position);
                        Selected_vas_type = (String) vas_hashmap.get("typeName");
                        Selected_vas_id = (String) vas_hashmap.get("typeId");
                        Log.e("selected typeName", Selected_vas_type);
                        Log.e("selected typeId", Selected_vas_id);
                        //TODO: add the price type here.
                        String price_value = vas_sub_type_price.get(Selected_vas_id);
                        if (price_value != null) {
                            tv_valet_service_price.setText(price_value + " SAR");
                            service_sub_type = true;
                        } else {
                            tv_valet_service_price.setText("----------");
                            service_sub_type = false;
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                }
        );*/
    }


    public void hideKeyBoard() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public JSONObject GetGenerateTicketDetailsAPI(String url, String json) {
        String Header_id, Header_value;
        Header_id = "Authorization";
        Header_value = "Bearer " + GlobalVariables.user_token;
        JSONObject jsonObjectResp = null;

        Log.e("Get Amount URL=>", url);

        try {

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, json);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .addHeader(Header_id, Header_value)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                jsonObjectResp = GetGenerateTicketDetailsAPIResponse(networkResp);
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            jsonObjectResp = GetGenerateTicketDetailsAPIResponse(err);
        }

        return jsonObjectResp;
    }

    public JSONObject GetGenerateTicketDetailsAPIResponse(final String str) {

        get_generate_ticket_details_response = null;
        try {
            get_generate_ticket_details_response = new JSONObject(str);

            Log.e("Get Amount Resp", String.valueOf(get_generate_ticket_details_response));
            String message = get_generate_ticket_details_response.getString("msg");
            int code = get_generate_ticket_details_response.getInt("code");


            runOnUiThread(() -> {
                if (code == 20) {
                    try {
                        JSONObject json = new JSONObject(str);
                        JSONObject jsonObject = json.getJSONObject("data");
                        JSONArray site_vas = jsonObject.getJSONArray("site_vas");

                        if (site_vas.length() == 0) {
                            Toast.makeText(this, "Shuttle service unavailable", Toast.LENGTH_SHORT).show();
                            valet_service_value.setText("---------");
                            status_tv.setText("-----");
                        } else {
                            for (int i = 0; i < site_vas.length(); i++) {
                                JSONObject site_vas_obj = site_vas.getJSONObject(i);
                                if (site_vas_obj.has("vas_types")) {
                                    String vasType = site_vas_obj.getString("vas_types");

                                    String subType = site_vas_obj.getString("vas_sub_type_id");
                                    if (vasType.equals("Shuttle")) {
                                        price = site_vas_obj.getDouble("vas_price");
                                        valet_service_value.setText(vasType);
                                        status_tv.setText("New");
                                        filled_exposed_service_sub_type.setText("Normal");
                                        vas_sub_type_price.put(subType, String.valueOf(price));
                                        isShuttleService = true;
                                        et_no_of_passenger.setEnabled(true);
                                        amount_value_tv.setText("" + String.format("%.2f",Integer.parseInt(et_no_of_passenger.getText().toString())*Double.valueOf(price)) + " SAR");
                                        if (price != null) {
                                            tv_valet_service_price.setText(Double.valueOf(price) + " SAR");
                                            service_sub_type = true;
                                        } else {
                                            tv_valet_service_price.setText("----------");
                                            service_sub_type = false;
                                        }


                                    }
                                }
                            }
                            if (!isShuttleService){
                                Toast.makeText(this, "Shuttle service unavailable", Toast.LENGTH_SHORT).show();
                                valet_service_value.setText("---------");
                                status_tv.setText("-----");
                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (code == 43) {
                    SharedPreferences prefs = getSharedPreferences(
                            "saved_data", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("login_status", "");
                    editor.putString("token", "");
                    editor.putString("role_id", "");
                    editor.putString("user_name", "");
                    editor.apply();
                    Toast.makeText(GenerateTicketActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(GenerateTicketActivity.this, LoadingActivity.class));

                } else {

                    ViewDialog alert = new ViewDialog();
                    alert.showDialog(GenerateTicketActivity.this, message);
                }

                GlobalVariables.progressDialog.dismiss();

            });
        } catch (Exception ex) {
            //  Log.e("Could not parse malformed JSON: \"" + json + "\"");
            try {
                get_generate_ticket_details_response = new JSONObject();
                get_generate_ticket_details_response.put("result", "failed");
                get_generate_ticket_details_response.put("data", str);
                get_generate_ticket_details_response.put("error", ex.getMessage());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        // tv_response.setText("ID:"+id+" and Name:"+name);
                        Toast.makeText(GenerateTicketActivity.this, "" + ex.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
            } catch (Exception exx) {
            }
            GlobalVariables.progressDialog.dismiss();
        }

        return get_generate_ticket_details_response;

    }

    public JSONObject GetSitesNameAPI(String url, String json) {
        String Header_id, Header_value;
        Header_id = "Authorization";
        Header_value = "Bearer " + GlobalVariables.user_token;
        Log.e("Site URL", url);
        JSONObject jsonObjectResp = null;

        try {

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, json);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .method("GET", null)
                    .addHeader(Header_id, Header_value)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                jsonObjectResp = GetSitesNameAPIResponse(networkResp);
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            jsonObjectResp = GetSitesNameAPIResponse(err);
        }

        return jsonObjectResp;
    }

    public JSONObject GetSitesNameAPIResponse(final String str) {

        sites_name_response = null;
        try {
            sites_name_response = new JSONObject(str);

            Log.e("Sites Name Response", String.valueOf(sites_name_response));

            String message = sites_name_response.getString("msg");
            int code = sites_name_response.getInt("code");

            runOnUiThread(() -> {

                if (code == 20) {

                    try {
                        JSONObject json = new JSONObject(str);

                        JSONArray jsonArray = json.getJSONArray("data");
                        site_id_array.add("");
                        site_name_array.add("Select site");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            Log.e("JsonObject", String.valueOf(jsonObject));
                            String site_heirarchy = jsonObject.getString("site_heirarchy");
                            String site_name = jsonObject.getString("name");
                            String id = jsonObject.getString("id");
                            String address = jsonObject.getString("address");

                            site_id_array.add(id);
                            site_name_array.add(site_name);
                        }

                        for (int i = 0; i < site_id_array.size(); i++) {

                            // creating an Object of HashMap class
                            site_name_hashmap = new HashMap<>();

                            // Data entry in HashMap
                            site_name_hashmap.put("siteId", site_id_array.get(i));
                            site_name_hashmap.put("siteName", site_name_array.get(i));

                            // adding the HashMap to the ArrayList
                            site_name_list.add(site_name_hashmap);


                        }

                        // Initialize array adapter
                        // set adapter
                        String[] from = {"siteId", "siteName"};

                        // creating an integer type array(to) which contains
                        // id of each View in each row of the list
                        // and this array(form) is the fifth parameter of the SimpleAdapter
                        int to[] = {R.id.textView1, R.id.textView};
                        site_adapter = new SimpleAdapter(getApplicationContext(), site_name_list, R.layout.site_name_dialoglist, from, to);
                        site_dropdown.setAdapter(site_adapter);


                        //This means only one value.
                        if (site_id_array.size() == 2) {

                            site_dropdown.setSelection(1);
                            site_name_hashmap = (HashMap<String, Object>) site_adapter.getItem(1);
                            Selected_SiteName = (String) site_name_hashmap.get("siteName");
                            Selected_SiteId = (String) site_name_hashmap.get("siteId");

                            String url = GlobalVariables.API_URL + "supervisor/sites/" + Selected_SiteId;
                            GlobalVariables.siteid = Selected_SiteId;

                            Thread thread_site_id = new Thread(() -> {
                                try {
                                    GetGenerateTicketDetailsAPI(url, "");
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            });
                            thread_site_id.start();
                        }

                        //Call for the service sub type
                        String vas_type_url = GlobalVariables.API_URL + "supervisor/vas_sub_type/3";
                        Thread thread = new Thread(() -> {
                            try {
                                GetVASType(vas_type_url, "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        });
                        thread.start();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (code == 43) {
                    SharedPreferences prefs = getSharedPreferences(
                            "saved_data", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("login_status", "");
                    editor.putString("token", "");
                    editor.putString("role_id", "");
                    editor.putString("user_name", "");
                    editor.apply();
                    Toast.makeText(GenerateTicketActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(GenerateTicketActivity.this, LoadingActivity.class));

                } else {
                    //  Toast.makeText(DisplayParkingActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                }

                Log.e("Checking=>", "");
                GlobalVariables.progressDialog.dismiss();

            });


        } catch (Exception ex) {
            //  Log.e("Could not parse malformed JSON: \"" + json + "\"");
            try {
                sites_name_response = new JSONObject();
                sites_name_response.put("result", "failed");
                sites_name_response.put("data", str);
                sites_name_response.put("error", ex.getMessage());
                runOnUiThread(() -> {
                    Toast.makeText(GenerateTicketActivity.this, "" + ex.getMessage(), Toast.LENGTH_SHORT).show();
                });
            } catch (Exception exx) {
            }
            GlobalVariables.progressDialog.dismiss();
        }

        return sites_name_response;

    }

    private JSONObject GetVASType(String vas_type_url, String s) {
        String Header_id, Header_value;
        Header_id = "Authorization";
        Header_value = "Bearer " + GlobalVariables.user_token;
        JSONObject jsonObjectResp = null;

        try {
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, s);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(vas_type_url)
                    .method("GET", null)
                    .addHeader(Header_id, Header_value)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                jsonObjectResp = GetVASTypeResponse(networkResp);
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            jsonObjectResp = GetVASTypeResponse(err);
        }

        return jsonObjectResp;
    }

    private JSONObject GetVASTypeResponse(String networkResp) {
        vas_type_response = null;
        try {
            vas_type_response = new JSONObject(networkResp);

            Log.e("vehicle type response", String.valueOf(vas_type_response));
            int code = vas_type_response.getInt("code");

            runOnUiThread(() -> {

                if (code == 20) {

                    try {
                        JSONObject json = new JSONObject(networkResp);

                        JSONArray jsonArray = json.getJSONArray("data");
                        vas_type_id_array.add("");
                        vas_type_name_array.add("Select type");
                        for (int i = 0; i < jsonArray.length(); i++) {

                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            String vehicle_type_id = jsonObject.getString("id");
                            String vehicle_type_name = jsonObject.getString("name");

                            vas_type_id_array.add(vehicle_type_id);
                            vas_type_name_array.add(vehicle_type_name);

                        }
                        for (int i = 0; i < vas_type_id_array.size(); i++) {

                            // creating an Object of HashMap class
                            vas_hashmap = new HashMap<>();

                            // Data entry in HashMap
                            vas_hashmap.put("typeId", vas_type_id_array.get(i));
                            vas_hashmap.put("typeName", vas_type_name_array.get(i));

                            // adding the HashMap to the ArrayList
                            vas_type_list.add(vas_hashmap);
                        }

                        String[] from = {"typeId", "typeName"};

                        // creating an integer type array(to) which contains
                        // id of each View in each row of the list
                        // and this array(form) is the fifth parameter of the SimpleAdapter
                        int to[] = {R.id.textView1, R.id.textView};
                        vas_adapter = new SimpleAdapter(getApplicationContext(), vas_type_list, R.layout.vehicle_type_dialoglist, from, to);
                        /*service_type_spinner.setAdapter(vas_adapter);*/
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                } else if (code == 43) {
                    SharedPreferences prefs = getSharedPreferences(
                            "saved_data", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("login_status", "");
                    editor.putString("token", "");
                    editor.putString("role_id", "");
                    editor.putString("user_name", "");
                    editor.apply();
                    startActivity(new Intent(GenerateTicketActivity.this, LoadingActivity.class));

                } else {
                }

                Log.e("Checking=>", "");


            });


        } catch (Exception ex) {
            //  Log.e("Could not parse malformed JSON: \"" + json + "\"");
            try {
                vas_type_response = new JSONObject();
                vas_type_response.put("result", "failed");
                vas_type_response.put("data", networkResp);
                vas_type_response.put("error", ex.getMessage());
                runOnUiThread(() -> {
                    Toast.makeText(GenerateTicketActivity.this, "" + ex.getMessage(), Toast.LENGTH_SHORT).show();

                });
            } catch (Exception exx) {
            }
            GlobalVariables.progressDialog.dismiss();
        }

        return vas_type_response;
    }

    private void getWorkingHours() {
        String workingurl = GlobalVariables.API_URL + "sites/vas_assign_working_hours/" + GlobalVariables.siteid + "?vas_type_id=" + GlobalVariables.vas_type_id;

        Thread thread = new Thread(() -> {
            try {
                getWorkingHourRequest(workingurl, "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        thread.start();


    }

    private JSONObject getWorkingHourRequest(String workingurl, String s) {

        String Header_id, Header_value;
        Header_id = "Authorization";
        Header_value = "Bearer " + GlobalVariables.user_token;
        Log.e("working_hours", "" + workingurl);
        JSONObject jsonObjectResp = null;

        try {

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, s);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(workingurl)
                    .method("GET", null)
                    .addHeader(Header_id, Header_value)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                jsonObjectResp = getWorkingHoursResponse(networkResp);
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            jsonObjectResp = getWorkingHoursResponse(err);
        }

        return jsonObjectResp;
    }

    private JSONObject getWorkingHoursResponse(String networkResp) {
        working_hours_response = null;
        try {
            working_hours_response = new JSONObject(networkResp);

            Log.e("working hours response", String.valueOf(working_hours_response));
            int code = working_hours_response.getInt("code");
            String message = working_hours_response.getString("msg");

            runOnUiThread(() -> {
                if (code == 20) {
                    JSONObject json = null;
                    try {
                        json = new JSONObject(networkResp);
                        JSONArray jsonArray = json.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            int day = jsonObject.getInt("day");
                            String first_half_start = jsonObject.getString("first_half_start");
                            String first_half_end = jsonObject.getString("first_half_end");
                            String second_half_start = jsonObject.getString("second_half_start");
                            String second_half_end = jsonObject.getString("second_half_end");

                            WorkerHoursModel workerHoursModel = new WorkerHoursModel();
                            workerHoursModel.setDays(day);
                            workerHoursModel.setFirst_half_start(first_half_start);
                            workerHoursModel.setFirst_half_end(first_half_end);
                            workerHoursModel.setSecond_half_start(second_half_start);
                            workerHoursModel.setSecond_half_end(second_half_end);

                            worker_hour_list.add(workerHoursModel);
                        }
                        //TODO : Calculation need to done.
                        calculateTimeHours();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else if (code == 43) {
                    SharedPreferences prefs = getSharedPreferences(
                            "saved_data", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("login_status", "");
                    editor.putString("token", "");
                    editor.putString("role_id", "");
                    editor.putString("user_name", "");
                    editor.apply();
                    startActivity(new Intent(GenerateTicketActivity.this, LoadingActivity.class));

                } else {
                    ViewDialog alert = new ViewDialog();
                    alert.showDialog(GenerateTicketActivity.this, "Working hours API: " + message);
                    //Toast.makeText(MainActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                }
                Log.e("Checking=>", "");
            });

        } catch (Exception ex) {
            try {
                working_hours_response = new JSONObject();
                working_hours_response.put("result", "failed");
                working_hours_response.put("data", networkResp);
                working_hours_response.put("error", ex.getMessage());
                runOnUiThread(() -> {
                    Toast.makeText(GenerateTicketActivity.this, "" + ex.getMessage(), Toast.LENGTH_SHORT).show();

                });
            } catch (Exception exx) {
                Log.e("TAG", "getWorkingHoursResponse: ", exx);
            }
            GlobalVariables.progressDialog.dismiss();
        }

        return working_hours_response;
    }

    private void calculateTimeHours() {
        Date date = new Date();
        Log.e("TAG", "Date==>: " + date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int days_number = cal.get(Calendar.DAY_OF_WEEK) - 1;
        Log.e("TimeHours", "calculateTimeHours: " + days_number);

        boolean is_today_working = false;

        for (int i = 0; i < worker_hour_list.size(); i++) {
            if (worker_hour_list.get(i).getDays() == days_number) {
                is_today_working =true;
                try {
                    if (worker_hour_list.get(i).getFirst_half_start() != "null" ) {
                        Date first_half_start = input_date.parse(worker_hour_list.get(i).getFirst_half_start());
                        Date first_half_end = input_date.parse(worker_hour_list.get(i).getFirst_half_end());

                        String first_half_start_str = input_time.format(first_half_start);
                        String first_half_end_str = input_time.format(first_half_end);

                        String todaysdate_string = input_time.format(date);

                        Date todayDate = input_time.parse(todaysdate_string);
                        Date first_start = input_time.parse(first_half_start_str);
                        Date first_end = input_time.parse(first_half_end_str);

                        boolean is_fall_under_first_half = todayDate.after(first_start) && todayDate.before(first_end);
                        Log.e("isFall Under ==> ", "" + is_fall_under_first_half);

                        if (!is_fall_under_first_half) {
                            //Check the second half here
                            if (worker_hour_list.get(i).getSecond_half_start() != "null"){
                                Date second_half_start = input_date.parse(worker_hour_list.get(i).getSecond_half_start());
                                Date second_half_end = input_date.parse(worker_hour_list.get(i).getSecond_half_end());

                                String second_half_start_str = input_time.format(second_half_start);
                                String second_half_end_str = input_time.format(second_half_end);

                                Date second_start = input_time.parse(second_half_start_str);
                                Date second_end = input_time.parse(second_half_end_str);

                                boolean is_fall_under_second_half = todayDate.after(second_start) && todayDate.before(second_end);
                                Log.e("isFallSecondHalf ==> ", "" + is_fall_under_second_half);

                                if (!is_fall_under_second_half) {
                                    ViewDialog alert = new ViewDialog();
                                    alert.showDialog(GenerateTicketActivity.this, "Working Hours Closed");
                                } else {
                                    //call submit ticket
                                    sendTicketDetails();

//                                    SubmitTicketDetails();
                                }
                            }else{
                                ViewDialog alert = new ViewDialog();
                                alert.showDialog(GenerateTicketActivity.this, "Working Hours Closed");
                            }
                        } else{
                            //call submit ticket fall under in the first hours
                            sendTicketDetails();
//                            SubmitTicketDetails();
                        }
                    } else {
                        if (worker_hour_list.get(i).getSecond_half_start() != "null") {
                            //Check the second half here
                            Date second_half_start = input_date.parse(worker_hour_list.get(i).getSecond_half_start());
                            Date second_half_end = input_date.parse(worker_hour_list.get(i).getSecond_half_end());

                            String second_half_start_str = input_time.format(second_half_start);
                            String second_half_end_str = input_time.format(second_half_end);

                            Date second_start = input_time.parse(second_half_start_str);
                            Date second_end = input_time.parse(second_half_end_str);

                            String todaysdate_string = input_time.format(date);

                            Date todayDate = input_time.parse(todaysdate_string);

                            boolean is_fall_under_second_half = todayDate.after(second_start) && todayDate.before(second_end);
                            Log.e("isFallSecondHalf ==> ", "" + is_fall_under_second_half);

                            if (!is_fall_under_second_half) {
                                ViewDialog alert = new ViewDialog();
                                alert.showDialog(GenerateTicketActivity.this, "Working Hours Closed");
                            } else {
                                //call submit ticket
                                sendTicketDetails();
//                                SubmitTicketDetails();
                            }
                        }
                    }
                } catch (ParseException e) {
                    Log.e("TAG", "calculateTimeHours: " + e);
                }
                //Ending forcefully
                break;
            }
        }

        if (!is_today_working){
            ViewDialog alert = new ViewDialog();
            alert.showDialog(GenerateTicketActivity.this, "Today is not a Working Day.");
        }


    }
    private void sendTicketDetails() {

        Date today = new Date();
        SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZZZZ");
        SimpleDateFormat time = new SimpleDateFormat("HH:mm:ss");
        cur_date = date.format(today);
        cur_time = time.format(today);
        DateTime = cur_date;
        quantity = et_no_of_passenger.getText().toString();
        if (et_customer_mobile.getText().toString().equals("") && et_customer_name.getText().toString().equals("")){
            submit_add_enforcement_json = "{\n" +
                    "  \"site_id\": " + Selected_SiteId + ",\n" +
                    "  \"country_code_id\": \"" + generate_ticket_selected_country_code + "\",\n" +
                    "  \"start_time\": \"" + DateTime + "\",\n" +
                    "  \"vas_type_id\": " + Integer.parseInt("3") + ",\n" +
                    "  \"vas\":\n" + "[\n" +
                    "{\n" +
                    "  \"vas_type_id\": " + Integer.parseInt("3") + ",\n" +
                    "  \"qty\": " + quantity + ",\n" +
                    "  \"description\": \"" + "shuttle service" + "\",\n" +
                    "  \"vas_sub_type_id\": " + Integer.parseInt("7") + "\n" +
                    "}\n" +
                    "]\n" +
                    "}";
        }
        else{
            submit_add_enforcement_json = "{\n" +
                    "  \"site_id\": " + Selected_SiteId + ",\n" +
                    "  \"first_name\": \"" + et_customer_name.getText().toString() + "\",\n" +
                    "  \"country_code_id\": \"" + generate_ticket_selected_country_code + "\",\n" +
                    "  \"mobile\": \"" + et_customer_mobile.getText().toString() + "\",\n" +
                    "  \"start_time\": \"" + DateTime + "\",\n" +
                    "  \"vas_type_id\": " + Integer.parseInt("3") + ",\n" +
                    "  \"vas\":\n" + "[\n" +
                    "{\n" +
                    "  \"vas_type_id\": " + Integer.parseInt("3") + ",\n" +
                    "  \"qty\": " + quantity + ",\n" +
                    "  \"description\": \"" + "shuttle service" + "\",\n" +
                    "  \"vas_sub_type_id\": " + Integer.parseInt("7") + "\n" +
                    "}\n" +
                    "]\n" +
                    "}";

        }


        Log.e("ticket_json==>",submit_add_enforcement_json);
        //Generate the Ticket.
        generateTicket(submit_add_enforcement_json);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_print_ticket:
                SubmitTicketDetails();
                break;
        }
    }

    private void SubmitTicketDetails() {
        if (Selected_SiteId.equals("")) {
            ViewDialog alert = new ViewDialog();
            alert.showDialog(GenerateTicketActivity.this, "Please Select Site Name");
        }/* else if (et_customer_name.getText().toString().equals("")) {
            ViewDialog alert = new ViewDialog();
            alert.showDialog(GenerateTicketActivity.this, "Please enter Customer Name");
        } else if (et_customer_mobile.getText().toString().equals("")) {
            ViewDialog alert = new ViewDialog();
            alert.showDialog(GenerateTicketActivity.this, "Please enter Mobile Number");
        } else if (et_customer_mobile.getText().length() < 9) {
            ViewDialog alert = new ViewDialog();
            alert.showDialog(GenerateTicketActivity.this, "Please enter valid Mobile Number");
        }*/ else if (!service_sub_type) {
            ViewDialog alert = new ViewDialog();
            alert.showDialog(GenerateTicketActivity.this, "Service Sub Type is not Available");
        } else if (et_no_of_passenger.getText().toString().equals("")) {
            ViewDialog alert = new ViewDialog();
            alert.showDialog(GenerateTicketActivity.this, "Please Enter the number of Passenger");
        } else if (Integer.parseInt(et_no_of_passenger.getText().toString()) == 0) {
            ViewDialog alert = new ViewDialog();
            alert.showDialog(GenerateTicketActivity.this, "Please Enter the Correct no of Passenger");
        }else {
            getWorkingHours();




//            Intent intent = new Intent(this, TicketPrintActivity.class);
//            intent.putExtra("submit_json", submit_add_enforcement_json);
//            intent.putExtra("selected_site_name", Selected_SiteName);
//            startActivity(intent);


        }


    }

    private void generateTicket(String submit_add_enforcement_json) {
        if (submit_add_enforcement_json != null) {
            String url = GlobalVariables.API_URL + "supervisor/generate-ticket-service-app";

            GlobalVariables.progressDialog = GlobalVariables.createProgressDialog(this);
            GlobalVariables.progressDialog.show();

            Thread thread = new Thread(() -> {
                try {
                    GenerateTicketAPI(url, submit_add_enforcement_json);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            thread.start();
        } else {
            Toast.makeText(this, "No data Found", Toast.LENGTH_SHORT).show();
        }
    }

    private JSONObject GenerateTicketAPI(String url, String submit_add_enforcement_json) {
        Log.e("URL=>", url);
        Log.e("json", submit_add_enforcement_json);
        Log.e("user_token", GlobalVariables.user_token);
        String Header_id, Header_value;
        JSONObject jsonObjectResp = null;
        Header_id = "Authorization";
        Header_value = "Bearer " + GlobalVariables.user_token;

        try {
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, submit_add_enforcement_json);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .method("POST", body)
                    .addHeader(Header_id, Header_value)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                jsonObjectResp = GenerateTicketAPIResponse(networkResp);
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            jsonObjectResp = GenerateTicketAPIResponse(err);
        }
            return  jsonObjectResp;
    }

    private JSONObject GenerateTicketAPIResponse(String networkResp) {
        generate_ticket_details_submit_response = null;
        try {
            generate_ticket_details_submit_response = new JSONObject(networkResp);

            int code = generate_ticket_details_submit_response.getInt("code");
            String message = generate_ticket_details_submit_response.getString("msg");
            Log.e("Code=>", String.valueOf(code));
            Log.e("Ticket Response=>", String.valueOf(generate_ticket_details_submit_response));


            runOnUiThread(() -> {

                if (code == 20) {
                    try {
                        JSONObject json = new JSONObject(networkResp);

                        JSONObject jsonObject = json.getJSONObject("data");
                        String orderId = jsonObject.getString("id");
                        String Ticket_id = jsonObject.getString("pass_id");
                        String vehicle_no = jsonObject.getString("number_plate");
                        String date_time = jsonObject.getString("start_time");
                        String site_no = jsonObject.getString("site_id");
                        String reservation_type_id = jsonObject.getString("reservation_type_id");
                        GlobalVariables.reservation_type_id = reservation_type_id;
                        if (Selected_SiteName != null) {
                            GlobalVariables.after_ticket_generated_site_name = Selected_SiteName;
                        }
                        GlobalVariables.after_ticket_generated_ticket_id = Ticket_id;

                        if (date_time.equals("") || date_time.equals(null)) {
                        } else {
                            GlobalVariables.after_ticket_generated_datetime = date_time;
                        }
                        GlobalVariables.siteid = site_no;

                        Intent intent = new Intent(this,TicketPrintActivity.class);
                        startActivity(intent);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    Log.e("ERROR msg", message);

                } else if (code == 43) {
                    SharedPreferences prefs = getSharedPreferences(
                            "saved_data", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("login_status", "");
                    editor.putString("token", "");
                    editor.putString("role_id", "");
                    editor.putString("user_name", "");
                    editor.apply();
                    Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(this, LoadingActivity.class));

                } else {
                    Log.e("ERROR msg", message);
                    Toast.makeText(this, "" + message, Toast.LENGTH_SHORT).show();
                }

                Log.e("Checking=>", "");
                GlobalVariables.progressDialog.dismiss();

            });


        } catch (Exception ex) {
            try {
                generate_ticket_details_submit_response = new JSONObject();
                generate_ticket_details_submit_response.put("result", "failed");
                generate_ticket_details_submit_response.put("data", networkResp);
                generate_ticket_details_submit_response.put("error", ex.getMessage());
                runOnUiThread(() -> {
                    GlobalVariables.progressDialog.dismiss();
                    Toast.makeText(GenerateTicketActivity.this, "" + ex.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.e("ERROR msg", ex.getMessage());
                });
            } catch (Exception exx) {
            }
        }

        return generate_ticket_details_submit_response;

    }


}