package com.sps.shuttelserviceapp;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.sps.shuttelserviceapp.InternetCheck.ConnectionReceiver;
import com.sps.shuttelserviceapp.Models.WorkerHoursModel;
import com.sps.shuttelserviceapp.Scanner.ScanningCameraDialog;
import com.sps.shuttelserviceapp.Utils.GlobalVariables;
import com.sps.shuttelserviceapp.Utils.ViewDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;


public class HomeFragment extends Fragment implements View.OnClickListener, ConnectionReceiver.ReceiverListener {

    boolean isConnected;
    TextView tv_username;
    Button btn_generate_ticket, btn_scan_the_ticket, btn_submit;
    EditText edt_ticket_number;
    JSONObject ticket_response, status_response;
    Spinner status_spinner;
    String Selected_status, updated_status;
    String[] courses = {"Ready to take order", "not ready"};
    private ArrayList<WorkerHoursModel> worker_hour_list = new ArrayList<>();
    JSONObject working_hours_response , working_response;
    SimpleDateFormat input_date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    SimpleDateFormat input_time = new SimpleDateFormat("HH:mm:ss");


    public HomeFragment() {

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        tv_username = (TextView) getView().findViewById(R.id.tv_user_name);
        tv_username.setText(GlobalVariables.user_name);

        edt_ticket_number = (EditText) getView().findViewById(R.id.edt_ticket_number);

        SharedPreferences prefs = getActivity().getSharedPreferences(
                "saved_data", Context.MODE_PRIVATE);
        int readyStatus = prefs.getInt("readyOrder", 0);


        btn_submit = (Button) getView().findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(v -> {
            switch (v.getId()) {
                case R.id.btn_submit:
                    if (edt_ticket_number.getText().toString().isEmpty()) {
                        ViewDialog alert = new ViewDialog();
                        alert.showDialog(getActivity(), "Please Enter Ticket Number");
                    } else if (edt_ticket_number.getText().toString().length() > 19 ) {
                        ViewDialog alert = new ViewDialog();
                        alert.showDialog(getActivity(), "Invalid Ticket Number");
                    } else if (edt_ticket_number.getText().toString().length() < 21 ) {
                        ViewDialog alert = new ViewDialog();
                        alert.showDialog(getActivity(), "Invalid Ticket Number");
                    }else if(readyStatus == 1){
                        //  Toast.makeText(getContext(),"Please Change the Status",Toast.LENGTH_SHORT).show();
                        ViewDialog alert = new ViewDialog();
                        alert.showDialog(getActivity(), "Please Change the Status");
                    }else {
                        checkConnection();
                        if(isConnected){
                            getWorkingHours();
                        }else{
                            Toast.makeText(getActivity(),"Please Check your Internet Connection !",Toast.LENGTH_LONG).show();
                        }
                        /*String et_value = edt_ticket_number.getText().toString();*//*
                        GlobalVariables.ticket_number = edt_ticket_number.getText().toString();
                        edt_ticket_number.setText(GlobalVariables.ticket_number);

                        String url = GlobalVariables.API_URL + "cashier/reservations-by-pass-id/" + GlobalVariables.ticket_number;
                        GlobalVariables.progressDialog = GlobalVariables.createProgressDialog(getActivity());
                        GlobalVariables.progressDialog.show();
                        Thread thread = new Thread(() -> {
                            try {
                                GetTicketNumber(url, "");
                            } finally {

                            }
                        });
                        thread.start();*/
                    }
                    break;
            }
        });

        btn_generate_ticket = (Button) getView().findViewById(R.id.btn_generate_ticket);
        btn_generate_ticket.setOnClickListener(this);

        btn_scan_the_ticket = (Button) getView().findViewById(R.id.btn_scan_the_ticket);
        btn_scan_the_ticket.setOnClickListener(this);
        String user_name = prefs.getString("user_name", "");
        SharedPreferences.Editor editor = prefs.edit();

        status_spinner = getView().findViewById(R.id.status_spinner);
        status_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view,
                                       int i, long l) {
                Selected_status = String.valueOf(adapterView.getItemAtPosition(i));
                Log.e("item", Selected_status);
                if (Selected_status.equals("Ready to take order")) {
                    updated_status = "true";
                    editor.putInt("readyOrder", 0);
                    editor.apply();
                } else {
                    updated_status = "false";
                    editor.putInt("readyOrder", 1);
                    editor.apply();
                }

                String url = GlobalVariables.API_URL + "worker/change-status?is_online=" + updated_status + "&location=12.287155795841672,76.62332738527618";

                Thread thread = new Thread(() -> {
                    try {
                        Status(url, "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });
                thread.start();
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub
            }
        });

        ArrayAdapter ad = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, courses);

        // set simple layout resource file
        // for each item of spinner
        ad.setDropDownViewResource(
                android.R.layout.simple_spinner_dropdown_item);

        // Set the ArrayAdapter (ad) data on the
        // Spinner which binds data to spinner
        status_spinner.setAdapter(ad);
        status_spinner.setSelection(prefs.getInt("readyOrder", 0));


    }

    private void getWorkingHours() {
        String workingurl = GlobalVariables.API_URL + "sites/vas_assign_working_hours/" + GlobalVariables.siteid + "?vas_type_id=" + GlobalVariables.vas_type_id;

        Thread thread = new Thread(() -> {
            try {
                getWorkingHourRequest(workingurl, "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        thread.start();


    }

    private void WorkingHours() {
        Log.e("site id",GlobalVariables.siteid);
        String workingUrl = GlobalVariables.API_URL + "sites/vas_assign_working_hours/" + GlobalVariables.siteid + "?vas_type_id=" + GlobalVariables.vas_type_id;

        Thread thread = new Thread(() -> {
            try {
                WorkingHourRequest(workingUrl, "");
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        thread.start();


    }

    private JSONObject getWorkingHourRequest(String workingurl, String s) {

        String Header_id, Header_value;
        Header_id = "Authorization";
        Header_value = "Bearer " + GlobalVariables.user_token;
        Log.e("working_hours", "" + workingurl);
        JSONObject jsonObjectResp = null;

        try {

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, s);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(workingurl)
                    .method("GET", null)
                    .addHeader(Header_id, Header_value)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                jsonObjectResp = getWorkingHoursResponse(networkResp);
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            jsonObjectResp = getWorkingHoursResponse(err);
        }

        return jsonObjectResp;
    }

    private JSONObject getWorkingHoursResponse(String networkResp) {
        working_hours_response = null;
        try {
            working_hours_response = new JSONObject(networkResp);

            Log.e("working hours response", String.valueOf(working_hours_response));
            int code = working_hours_response.getInt("code");
            String message = working_hours_response.getString("msg");

            getActivity().runOnUiThread(() -> {
                if (code == 20) {
                    JSONObject json = null;
                    try {
                        json = new JSONObject(networkResp);
                        JSONArray jsonArray = json.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            int day = jsonObject.getInt("day");
                            String first_half_start = jsonObject.getString("first_half_start");
                            String first_half_end = jsonObject.getString("first_half_end");
                            String second_half_start = jsonObject.getString("second_half_start");
                            String second_half_end = jsonObject.getString("second_half_end");

                            WorkerHoursModel workerHoursModel = new WorkerHoursModel();
                            workerHoursModel.setDays(day);
                            workerHoursModel.setFirst_half_start(first_half_start);
                            workerHoursModel.setFirst_half_end(first_half_end);
                            workerHoursModel.setSecond_half_start(second_half_start);
                            workerHoursModel.setSecond_half_end(second_half_end);

                            worker_hour_list.add(workerHoursModel);
                        }
                        //TODO : Calculation need to done.
                        calculateTimeHours();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else if (code == 43) {
                    SharedPreferences prefs = getActivity().getSharedPreferences(
                            "saved_data", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("login_status", "");
                    editor.putString("token", "");
                    editor.putString("role_id", "");
                    editor.putString("user_name", "");
                    editor.apply();
                    startActivity(new Intent(getActivity(), LoadingActivity.class));

                } else {
                    ViewDialog alert = new ViewDialog();
                    alert.showDialog(getActivity(), "Working hours API: " + message);
                    //Toast.makeText(MainActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                }
                Log.e("Checking=>", "");
            });

        } catch (Exception ex) {
            try {
                working_hours_response = new JSONObject();
                working_hours_response.put("result", "failed");
                working_hours_response.put("data", networkResp);
                working_hours_response.put("error", ex.getMessage());
                getActivity().runOnUiThread(() -> {
                    Toast.makeText(getActivity(), "" + ex.getMessage(), Toast.LENGTH_SHORT).show();

                });
            } catch (Exception exx) {
                Log.e("TAG", "getWorkingHoursResponse: ", exx);
            }
            GlobalVariables.progressDialog.dismiss();
        }

        return working_hours_response;
    }

    private void calculateTimeHours() {
        Date date = new Date();
        Log.e("TAG", "Date==>: " + date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int days_number = cal.get(Calendar.DAY_OF_WEEK) - 1;
        Log.e("TimeHours", "calculateTimeHours: " + days_number);

        boolean is_today_working = false;

        for (int i = 0; i < worker_hour_list.size(); i++) {
            if (worker_hour_list.get(i).getDays() == days_number) {
                is_today_working =true;
                try {
                    if (worker_hour_list.get(i).getFirst_half_start() != "null" ) {
                        Date first_half_start = input_date.parse(worker_hour_list.get(i).getFirst_half_start());
                        Date first_half_end = input_date.parse(worker_hour_list.get(i).getFirst_half_end());

                        String first_half_start_str = input_time.format(first_half_start);
                        String first_half_end_str = input_time.format(first_half_end);

                        String todaysdate_string = input_time.format(date);

                        Date todayDate = input_time.parse(todaysdate_string);
                        Date first_start = input_time.parse(first_half_start_str);
                        Date first_end = input_time.parse(first_half_end_str);

                        boolean is_fall_under_first_half = todayDate.after(first_start) && todayDate.before(first_end);
                        Log.e("isFall Under ==> ", "" + is_fall_under_first_half);

                        if (!is_fall_under_first_half) {
                            //Check the second half here
                            if (worker_hour_list.get(i).getSecond_half_start() != "null"){
                                Date second_half_start = input_date.parse(worker_hour_list.get(i).getSecond_half_start());
                                Date second_half_end = input_date.parse(worker_hour_list.get(i).getSecond_half_end());

                                String second_half_start_str = input_time.format(second_half_start);
                                String second_half_end_str = input_time.format(second_half_end);

                                Date second_start = input_time.parse(second_half_start_str);
                                Date second_end = input_time.parse(second_half_end_str);

                                boolean is_fall_under_second_half = todayDate.after(second_start) && todayDate.before(second_end);
                                Log.e("isFallSecondHalf ==> ", "" + is_fall_under_second_half);

                                if (!is_fall_under_second_half) {
                                    ViewDialog alert = new ViewDialog();
                                    alert.showDialog(getActivity(), "Working Hours Closed");
                                } else {
                                    //call submit ticket
                                    /*String et_value = edt_ticket_number.getText().toString();*/
                                    GlobalVariables.ticket_number = edt_ticket_number.getText().toString();
                                    edt_ticket_number.setText(GlobalVariables.ticket_number);

                                    String url = GlobalVariables.API_URL + "cashier/reservations-by-pass-id/" + GlobalVariables.ticket_number;
                                    GlobalVariables.progressDialog = GlobalVariables.createProgressDialog(getActivity());
                                    GlobalVariables.progressDialog.show();
                                    Thread thread = new Thread(() -> {
                                        try {
                                            GetTicketNumber(url, "");
                                        } finally {

                                        }
                                    });
                                    thread.start();

//                                    SubmitTicketDetails();
                                }
                            }else{
                                ViewDialog alert = new ViewDialog();
                                alert.showDialog(getActivity(), "Working Hours Closed");
                            }
                        } else{
                            //call submit ticket fall under in the first hours
                            /*String et_value = edt_ticket_number.getText().toString();*/
                            GlobalVariables.ticket_number = edt_ticket_number.getText().toString();
                            edt_ticket_number.setText(GlobalVariables.ticket_number);

                            String url = GlobalVariables.API_URL + "cashier/reservations-by-pass-id/" + GlobalVariables.ticket_number;
                            GlobalVariables.progressDialog = GlobalVariables.createProgressDialog(getActivity());
                            GlobalVariables.progressDialog.show();
                            Thread thread = new Thread(() -> {
                                try {
                                    GetTicketNumber(url, "");
                                } finally {

                                }
                            });
                            thread.start();
//                            SubmitTicketDetails();
                        }
                    } else {
                        if (worker_hour_list.get(i).getSecond_half_start() != "null") {
                            //Check the second half here
                            Date second_half_start = input_date.parse(worker_hour_list.get(i).getSecond_half_start());
                            Date second_half_end = input_date.parse(worker_hour_list.get(i).getSecond_half_end());

                            String second_half_start_str = input_time.format(second_half_start);
                            String second_half_end_str = input_time.format(second_half_end);

                            Date second_start = input_time.parse(second_half_start_str);
                            Date second_end = input_time.parse(second_half_end_str);

                            String todaysdate_string = input_time.format(date);

                            Date todayDate = input_time.parse(todaysdate_string);

                            boolean is_fall_under_second_half = todayDate.after(second_start) && todayDate.before(second_end);
                            Log.e("isFallSecondHalf ==> ", "" + is_fall_under_second_half);

                            if (!is_fall_under_second_half) {
                                ViewDialog alert = new ViewDialog();
                                alert.showDialog(getActivity(), "Working Hours Closed");
                            } else {
                                //call submit ticket
                                /*String et_value = edt_ticket_number.getText().toString();*/
                                GlobalVariables.ticket_number = edt_ticket_number.getText().toString();
                                edt_ticket_number.setText(GlobalVariables.ticket_number);

                                String url = GlobalVariables.API_URL + "cashier/reservations-by-pass-id/" + GlobalVariables.ticket_number;
                                GlobalVariables.progressDialog = GlobalVariables.createProgressDialog(getActivity());
                                GlobalVariables.progressDialog.show();
                                Thread thread = new Thread(() -> {
                                    try {
                                        GetTicketNumber(url, "");
                                    } finally {

                                    }
                                });
                                thread.start();
//                                SubmitTicketDetails();
                            }
                        }
                    }
                } catch (ParseException e) {
                    Log.e("TAG", "calculateTimeHours: " + e);
                }
                //Ending forcefully
                break;
            }
        }

        if (!is_today_working){
            ViewDialog alert = new ViewDialog();
            alert.showDialog(getActivity(), "Today is not a Working Day.");
        }


    }

    private JSONObject WorkingHourRequest(String workingUrl, String s) {

        String Header_id, Header_value;
        Header_id = "Authorization";
        Header_value = "Bearer " + GlobalVariables.user_token;
        Log.e("working_hours", "" + workingUrl);
        JSONObject jsonObjectResp = null;

        try {

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, s);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(workingUrl)
                    .method("GET", null)
                    .addHeader(Header_id, Header_value)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                jsonObjectResp = WorkingHoursResponse(networkResp);
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            jsonObjectResp = WorkingHoursResponse(err);
        }

        return jsonObjectResp;
    }

    private JSONObject WorkingHoursResponse(String networkResp) {
        working_response = null;
        try {
            working_response = new JSONObject(networkResp);

            Log.e("working  response", String.valueOf(working_response));
            int code = working_response.getInt("code");
            String message = working_response.getString("msg");

            getActivity().runOnUiThread(() -> {
                if (code == 20) {
                    JSONObject json = null;
                    try {
                        json = new JSONObject(networkResp);
                        JSONArray jsonArray = json.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            int day = jsonObject.getInt("day");
                            String first_half_start = jsonObject.getString("first_half_start");
                            String first_half_end = jsonObject.getString("first_half_end");
                            String second_half_start = jsonObject.getString("second_half_start");
                            String second_half_end = jsonObject.getString("second_half_end");

                            WorkerHoursModel workerHoursModel = new WorkerHoursModel();
                            workerHoursModel.setDays(day);
                            workerHoursModel.setFirst_half_start(first_half_start);
                            workerHoursModel.setFirst_half_end(first_half_end);
                            workerHoursModel.setSecond_half_start(second_half_start);
                            workerHoursModel.setSecond_half_end(second_half_end);

                            worker_hour_list.add(workerHoursModel);
                        }
                        //TODO : Calculation need to done.
                        timeCalculateHours();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else if (code == 43) {
                    SharedPreferences prefs = getActivity().getSharedPreferences(
                            "saved_data", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("login_status", "");
                    editor.putString("token", "");
                    editor.putString("role_id", "");
                    editor.putString("user_name", "");
                    editor.apply();
                    startActivity(new Intent(getActivity(), LoadingActivity.class));


                } else {
                    ViewDialog alert = new ViewDialog();
                    alert.showDialog(getActivity(), "Working hours API: " + message);
                    //Toast.makeText(MainActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                }
                Log.e("Checking=>", "");
            });

        } catch (Exception ex) {
            try {
                working_hours_response = new JSONObject();
                working_hours_response.put("result", "failed");
                working_hours_response.put("data", networkResp);
                working_hours_response.put("error", ex.getMessage());
                getActivity().runOnUiThread(() -> {
                    Toast.makeText(getActivity(), "" + ex.getMessage(), Toast.LENGTH_SHORT).show();

                });
            } catch (Exception exx) {
                Log.e("TAG", "WorkingHoursResponse: ", exx);
            }
            GlobalVariables.progressDialog.dismiss();
        }

        return working_response;
    }

    private void timeCalculateHours() {
        Date date = new Date();
        Log.e("TAG", "Date==>: " + date);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int days_number = cal.get(Calendar.DAY_OF_WEEK) - 1;
        Log.e("TimeHours", "calculateTimeHours: " + days_number);

        boolean is_today_working = false;

        for (int i = 0; i < worker_hour_list.size(); i++) {
            if (worker_hour_list.get(i).getDays() == days_number) {
                is_today_working =true;
                try {
                    if (worker_hour_list.get(i).getFirst_half_start() != "null" ) {
                        Date first_half_start = input_date.parse(worker_hour_list.get(i).getFirst_half_start());
                        Date first_half_end = input_date.parse(worker_hour_list.get(i).getFirst_half_end());

                        String first_half_start_str = input_time.format(first_half_start);
                        String first_half_end_str = input_time.format(first_half_end);

                        String todaysdate_string = input_time.format(date);

                        Date todayDate = input_time.parse(todaysdate_string);
                        Date first_start = input_time.parse(first_half_start_str);
                        Date first_end = input_time.parse(first_half_end_str);

                        boolean is_fall_under_first_half = todayDate.after(first_start) && todayDate.before(first_end);
                        Log.e("isFall Under ==> ", "" + is_fall_under_first_half);

                        if (!is_fall_under_first_half) {
                            //Check the second half here
                            if (worker_hour_list.get(i).getSecond_half_start() != "null"){
                                Date second_half_start = input_date.parse(worker_hour_list.get(i).getSecond_half_start());
                                Date second_half_end = input_date.parse(worker_hour_list.get(i).getSecond_half_end());

                                String second_half_start_str = input_time.format(second_half_start);
                                String second_half_end_str = input_time.format(second_half_end);

                                Date second_start = input_time.parse(second_half_start_str);
                                Date second_end = input_time.parse(second_half_end_str);

                                boolean is_fall_under_second_half = todayDate.after(second_start) && todayDate.before(second_end);
                                Log.e("isFallSecondHalf ==> ", "" + is_fall_under_second_half);

                                if (!is_fall_under_second_half) {
                                    ViewDialog alert = new ViewDialog();
                                    alert.showDialog(getActivity(), "Working Hours Closed");
                                } else {
                                    //call submit ticket
                                    /*String et_value = edt_ticket_number.getText().toString();*/

                                    ScanningCameraDialog scanningCameraDialog = new ScanningCameraDialog();
                                    scanningCameraDialog.showDialog(getActivity());
//                                    SubmitTicketDetails();
                                }
                            }else{
                                ViewDialog alert = new ViewDialog();
                                alert.showDialog(getActivity(), "Working Hours Closed");
                            }
                        } else{
                            //call submit ticket fall under in the first hours
                            /*String et_value = edt_ticket_number.getText().toString();*/
                            ScanningCameraDialog scanningCameraDialog = new ScanningCameraDialog();
                            scanningCameraDialog.showDialog(getActivity());
//                            SubmitTicketDetails();
                        }
                    } else {
                        if (worker_hour_list.get(i).getSecond_half_start() != "null") {
                            //Check the second half here
                            Date second_half_start = input_date.parse(worker_hour_list.get(i).getSecond_half_start());
                            Date second_half_end = input_date.parse(worker_hour_list.get(i).getSecond_half_end());

                            String second_half_start_str = input_time.format(second_half_start);
                            String second_half_end_str = input_time.format(second_half_end);

                            Date second_start = input_time.parse(second_half_start_str);
                            Date second_end = input_time.parse(second_half_end_str);

                            String todaysdate_string = input_time.format(date);

                            Date todayDate = input_time.parse(todaysdate_string);

                            boolean is_fall_under_second_half = todayDate.after(second_start) && todayDate.before(second_end);
                            Log.e("isFallSecondHalf ==> ", "" + is_fall_under_second_half);

                            if (!is_fall_under_second_half) {
                                ViewDialog alert = new ViewDialog();
                                alert.showDialog(getActivity(), "Working Hours Closed");
                            } else {
                                //call submit ticket
                                /*String et_value = edt_ticket_number.getText().toString();*/
                                ScanningCameraDialog scanningCameraDialog = new ScanningCameraDialog();
                                scanningCameraDialog.showDialog(getActivity());
//                                SubmitTicketDetails();
                            }
                        }
                    }
                } catch (ParseException e) {
                    Log.e("TAG", "calculateTimeHours: " + e);
                }
                //Ending forcefully
                break;
            }
        }

        if (!is_today_working){
            ViewDialog alert = new ViewDialog();
            alert.showDialog(getActivity(), "Today is not a Working Day.");
        }


    }

    private JSONObject Status(String url, String json) {
        String Header_id, Header_value;
        Header_id = "Authorization";
        Header_value = "Bearer " + GlobalVariables.user_token;
        Log.e("status URL", String.valueOf(url));
        JSONObject jsonObjectResp = null;
        try {
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, json);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .method("POST", body)
                    .addHeader(Header_id, Header_value)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                jsonObjectResp = GetStatusResponse(networkResp);
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            jsonObjectResp = GetStatusResponse(err);
        }
        return jsonObjectResp;
    }

    private JSONObject GetStatusResponse(String str) {
        status_response = null;

        try {
            status_response = new JSONObject(str);
            String message = status_response.getString("msg");
            int code = status_response.getInt("code");
            Log.e("ready to take order", String.valueOf(status_response));
            getActivity().runOnUiThread(() -> {

                if (code == 20) {

                } else if (code == 43) {
                    SharedPreferences prefs = getActivity().getSharedPreferences(
                            "saved_data", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("login_status", "");
                    editor.putString("token", "");
                    editor.putString("role_id", "");
                    editor.putString("user_name", "");
                    editor.apply();
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getActivity(), LoadingActivity.class));

                } else {
                    ViewDialog alert = new ViewDialog();
                    alert.showDialog(getActivity(), message);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }


    public JSONObject GetTicketNumber(String url, String json) {
        String Header_id, Header_value;
        Header_id = "Authorization";
        Header_value = "Baerer " + GlobalVariables.user_token;
        JSONObject jsonObjectResp = null;

        try {

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, json);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .method("GET", null)
                    .addHeader(Header_id, Header_value)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                jsonObjectResp = TicketNumberResponse(networkResp);
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            jsonObjectResp = TicketNumberResponse(err);
        }

        return jsonObjectResp;
    }

    public JSONObject TicketNumberResponse(final String str) {
        ticket_response = null;
        try {
            ticket_response = new JSONObject(str);
            Log.e("Ticket Response=>", String.valueOf(ticket_response));

            String message = ticket_response.getString("msg");
            int code = ticket_response.getInt("code");


            getActivity().runOnUiThread(() -> {

                if (code == 20) {
                    try {
                        JSONObject json = new JSONObject(str);
                        if (json.has("data") && !json.isNull("data")) {
                            JSONObject jsonObject = json.getJSONObject("data");
                            if (jsonObject.has("result") && !jsonObject.isNull("result")) {
                                JSONObject result = jsonObject.getJSONObject("result");
                                if (result.has("vas_orders") && !result.isNull("vas_orders")) {
                                JSONArray jsonArray = result.getJSONArray("vas_orders");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    if (jsonObject1.has("vas_types") && !jsonObject1.isNull("vas_types")){
                                        JSONObject vas_types = jsonObject1.getJSONObject("vas_types");
                                        Log.e("Checking = 54 ", String.valueOf(vas_types));
                                        String vas_type_id = vas_types.getString("id");
                                        if (vas_type_id != null && vas_type_id == "24" && vas_type_id == "144" && vas_type_id == "2"){
                                            ViewDialog alert = new ViewDialog();
                                            alert.showDialog(getActivity(), "This Ticket doesn't have Shuttle Service");
                                            GlobalVariables.progressDialog.dismiss();
                                            return;
                                        }
                                        if (jsonObject1.has("vas_order_status_types") && !jsonObject1.isNull("vas_order_status_types")) {
                                            JSONObject vas_order_status_types = jsonObject1.getJSONObject("vas_order_status_types");
                                            String order_id = vas_order_status_types.getString("id");
                                            String status = vas_order_status_types.getString("name");
                                            if (order_id != null && order_id.equals("3")) {
                                                ViewDialog alert = new ViewDialog();
                                                alert.showDialog(getActivity(), "The Order is Completed");
                                                GlobalVariables.progressDialog.dismiss();
                                                return;
                                            }
                                        }
                                    }


                                }
                                }
                            }
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    Intent submit = new Intent(getActivity(),OrderDetailsActivity.class);
                    getActivity().startActivity(submit);
                 //   getActivity().finish();

                } else if (code == 43) {
                    SharedPreferences prefs = getActivity().getSharedPreferences(
                            "saved_data", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("login_status", "");
                    editor.putString("token", "");
                    editor.putString("role_id", "");
                    editor.putString("user_name", "");
                    editor.apply();
//                        Toast.makeText(GenerateTicketActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getActivity(), LoadingActivity.class));

                } else {
                    ViewDialog alert = new ViewDialog();
                    alert.showDialog(getActivity(), message);
                    //Toast.makeText(MainActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                }

                GlobalVariables.progressDialog.dismiss();

            });


        } catch (Exception ex) {
            //  Log.e("Could not parse malformed JSON: \"" + json + "\"");
            try {
                ticket_response = new JSONObject();
                ticket_response.put("result", "failed");
                ticket_response.put("data", str);
                ticket_response.put("error", ex.getMessage());
                getActivity().runOnUiThread(() -> {
                    Toast.makeText(getActivity(), "" + ex.getMessage(), Toast.LENGTH_SHORT).show();
                });
            } catch (Exception exx) {
            }
            GlobalVariables.progressDialog.dismiss();
        }

        return ticket_response;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_generate_ticket:
                SharedPreferences prefs = getActivity().getSharedPreferences(
                        "saved_data", Context.MODE_PRIVATE);
                int readyStatus = prefs.getInt("readyOrder", 0);
                if (readyStatus == 1){
                  //  Toast.makeText(getContext(),"Please Change the Status",Toast.LENGTH_SHORT).show();
                    ViewDialog alert = new ViewDialog();
                    alert.showDialog(getActivity(), "Please Change the Status");
                }else{
                    Intent intent = new Intent(getActivity(), GenerateTicketActivity.class);
                    getActivity().startActivity(intent);
                }
                break;

            case R.id.btn_scan_the_ticket:
                SharedPreferences prefs_1 = getActivity().getSharedPreferences(
                        "saved_data", Context.MODE_PRIVATE);
                int readyStatus_1 = prefs_1.getInt("readyOrder", 0);
                if (readyStatus_1 == 1){
                    //  Toast.makeText(getContext(),"Please Change the Status",Toast.LENGTH_SHORT).show();
                    ViewDialog alert = new ViewDialog();
                    alert.showDialog(getActivity(), "Please Change the Status");
                }else{
                    WorkingHours();

                }
                break;
        }
    }

    private void checkConnection() {

        // initialize intent filter
        IntentFilter intentFilter = new IntentFilter();

        // add action
        intentFilter.addAction("android.new.conn.CONNECTIVITY_CHANGE");

        // register receiver
        getActivity().registerReceiver(new ConnectionReceiver(), intentFilter);

        // Initialize listener
        ConnectionReceiver.Listener = this;

        // Initialize connectivity manager
        ConnectivityManager manager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        // Initialize network info
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

        // get connection status
        isConnected = networkInfo != null && networkInfo.isConnectedOrConnecting();


    }

    @Override
    public void onNetworkChange(boolean isConnected) {

    }
}