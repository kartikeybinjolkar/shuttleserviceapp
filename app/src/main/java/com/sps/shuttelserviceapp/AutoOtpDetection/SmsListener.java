package com.sps.shuttelserviceapp.AutoOtpDetection;

public interface SmsListener {
    public void messageReceived(String messageText);
}
