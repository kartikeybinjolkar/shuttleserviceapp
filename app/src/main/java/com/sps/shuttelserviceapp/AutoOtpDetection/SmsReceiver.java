package com.sps.shuttelserviceapp.AutoOtpDetection;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.provider.Telephony;
import android.telephony.SmsMessage;

public class SmsReceiver extends BroadcastReceiver {
    private static SmsListener mListener;
    Boolean b;
    String otp_message;
    @Override
    public void onReceive(Context context, Intent intent) {
        SmsMessage[] smsMessages = Telephony.Sms.Intents.getMessagesFromIntent(intent);
        for (SmsMessage smsMessage : smsMessages){
            String message_body = smsMessage.getMessageBody();
            otp_message = smsMessage.getMessageBody().replaceAll("[^0-9]", "");
            if (otp_message.length()==4) {
                mListener.messageReceived(otp_message); //
            }
        }
    }
    public static void bindListener(SmsListener listener) {
        mListener = listener;


    }
}