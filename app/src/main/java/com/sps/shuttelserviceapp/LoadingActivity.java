package com.sps.shuttelserviceapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.WindowManager;

import com.sps.shuttelserviceapp.NotificationService.NotificationMsgService;
import com.sps.shuttelserviceapp.NotificationService.SharedPreferencesManager;
import com.sps.shuttelserviceapp.Utils.GlobalVariables;

public class LoadingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);

        String android_id = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        GlobalVariables.android_id = android_id;

        SharedPreferences prefs = getSharedPreferences(
                "saved_data", Context.MODE_PRIVATE);
        String login_status = prefs.getString("login_status", "");
        String mobile = prefs.getString("user_mobile","");
        Log.e("Login Status",login_status);

        if (login_status.equals("log_in"))
        {
            SharedPreferencesManager.init(LoadingActivity.this);
            NotificationMsgService notifMsgServ = new NotificationMsgService();
            Log.i("Notification","Inside Loading");
            notifMsgServ.returnToken(mobile);

            Intent intent = new Intent(LoadingActivity.this, OrderListActivity.class);
            startActivity(intent);
            String token = prefs.getString("token", "");
            String role_id = prefs.getString("role_id", "");
            String user_name = prefs.getString("user_name", "");

            GlobalVariables.user_role_id = role_id;
            GlobalVariables.user_token = token;
            GlobalVariables.user_name = user_name;
           // finish();
        }else{
            startActivity(new Intent(LoadingActivity.this, LoginActivity.class));
            finish();
        }
    }
}