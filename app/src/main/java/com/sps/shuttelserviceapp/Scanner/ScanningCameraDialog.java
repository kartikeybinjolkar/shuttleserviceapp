package com.sps.shuttelserviceapp.Scanner;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.VIBRATE;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.sps.shuttelserviceapp.OrderDetailsActivity;
import com.sps.shuttelserviceapp.R;
import com.sps.shuttelserviceapp.Utils.GlobalVariables;
import com.sps.shuttelserviceapp.Utils.ViewDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;

import eu.livotov.labs.android.camview.ScannerLiveView;
import eu.livotov.labs.android.camview.scanner.decoder.zxing.ZXDecoder;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class ScanningCameraDialog {
    Activity mActivity;

    private ScannerLiveView camera;
    Dialog dialog;
    String cur_date,cur_time;
    DateFormat originalDateFormat,originalTimeFormat;
    DateFormat targetDateFormat,targetTimeFormat;
    Date from_date,from_time;
    String formatted_s_date,formatted_s_time;
    JSONObject get_reservation_id_response;
    public void showDialog(final Activity activity) {
        // super(activity);
        mActivity = activity;
        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.scanning_camera_dialog_layout);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        ImageView iv_close = dialog.findViewById(R.id.iv_swipe_down);
        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                camera.stopScanner();
                dialog.dismiss();

            }
        });
        LinearLayout ll_camera_scanner = dialog.findViewById(R.id.ll_camera_scanner);
        ll_camera_scanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                camera.stopScanner();
                dialog.dismiss();

            }
        });

        dialog.setOnKeyListener((dialog, keyCode, event) -> {
            if (keyCode == KeyEvent.KEYCODE_BACK){
                camera.stopScanner();
                dialog.dismiss();
                return true;
            }
            return false;
        });

        camera = (ScannerLiveView) dialog.findViewById(R.id.cam_view);

        if (checkPermission()) {
            // if permission is already granted display a toast message
            //Toast.makeText(mActivity, "Permission Granted..", Toast.LENGTH_SHORT).show();
        } else {
            requestPermission();
        }



        camera.setScannerViewEventListener(new ScannerLiveView.ScannerViewEventListener() {
            @Override
            public void onScannerStarted(ScannerLiveView scanner) {
                // method is called when scanner is started
                // Toast.makeText(mActivity, "Scanner Started", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onScannerStopped(ScannerLiveView scanner) {
                // method is called when scanner is stopped.
                // Toast.makeText(mActivity, "Scanner Stopped", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onScannerError(Throwable err) {
                // method is called when scanner gives some error.
                Toast.makeText(mActivity, "Scanner Error: " + err.getMessage(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onCodeScanned(String data) {
                // method is called when camera scans the
                // qr code and the data from qr code is
                // stored in data in string format.
                // scannedTV.setText(data);
                //Log.e("Scanner Value=>",data);
                // Toast.makeText(mActivity, "Scanner Value=> " + data, Toast.LENGTH_SHORT).show();
                String result = data;
                Log.e("Checking ticket ID=>",result);

//                GlobalVariables.pass_id = result.replace("http://iot.hyperthink.io:6054/ticket/", "");

                if (result.contains("t/")){
                    Log.e("Checking New ==>","");
                    int sepPos = result.indexOf("t/");
                    String qrvalue = result.substring(sepPos + 2);
                    Log.e("TAG", "onCodeScanned: "+ qrvalue );

                    String url = GlobalVariables.API_URL + "supervisor/scanner/"+qrvalue + "?vas_type_id=3";
//                    String url = GlobalVariables.API_URL + "supervisor/scanner/1664888682676";
                    if (GlobalVariables.progressDialog == null) {
                        GlobalVariables.progressDialog = GlobalVariables.createProgressDialog(mActivity);
                        GlobalVariables.progressDialog.show();
                    } else {
                        GlobalVariables.progressDialog = GlobalVariables.createProgressDialog(mActivity);
                        GlobalVariables.progressDialog.show();
                    }

                    Thread thread = new Thread(() -> {
                        try {
                            GetTicketIdDetailsAPI(url, "");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                    thread.start();
                }/*else if(result.contains("invoice/")){
                    Log.e("Checking ==>","");
                    int sepPos = result.indexOf("e/");
                    String qrvalue = result.substring(sepPos + 2);
                    Log.e("TAG", "onCodeScanned: "+ qrvalue );

                    String url = GlobalVariables.API_URL + "supervisor/scanner/"+qrvalue + "?vas_type_id=3";
//                    String url = GlobalVariables.API_URL + "supervisor/scanner/1664888682676";
                    if (GlobalVariables.progressDialog == null) {
                        GlobalVariables.progressDialog = GlobalVariables.createProgressDialog(mActivity);
                        GlobalVariables.progressDialog.show();
                    } else {
                        GlobalVariables.progressDialog = GlobalVariables.createProgressDialog(mActivity);
                        GlobalVariables.progressDialog.show();
                    }

                    Thread thread = new Thread(() -> {
                        try {
                            GetTicketIdDetailsAPI(url, "");
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                    thread.start();

                }*/
                else{
                    AlertDialog.Builder alert = new AlertDialog.Builder(mActivity);
                    alert.setMessage("Invalid QR Code").setPositiveButton("Close", null);
                    AlertDialog alert1 = alert.create();
                    alert1.show();
                }

            }
        });

        // dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//            WindowManager.LayoutParams wmlp = dialog.getWindow().getAttributes();
//            wmlp.gravity = Gravity.BOTTOM | Gravity.LEFT;
//            //wmlp.x = 80;   //x position
//            wmlp.y = 150;   //y position
//            dialog.getWindow().setGravity(Gravity.BOTTOM);
//            dialog.show();


        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(dialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM;
        lp.windowAnimations = R.style.DialogAnimation;
        dialog.getWindow().setAttributes(lp);
        dialog.show();
        ZXDecoder decoder = new ZXDecoder();
        // 0.5 is the area where we have
        // to place red marker for scanning.
        decoder.setScanAreaPercent(0.9);
        // below method will set secoder to camera.
        camera.setDecoder(decoder);
        camera.startScanner();

    }
    private boolean checkPermission() {
        // here we are checking two permission that is vibrate
        // and camera which is granted by user and not.
        // if permission is granted then we are returning
        // true otherwise false.
        int camera_permission = ContextCompat.checkSelfPermission(mActivity, CAMERA);
        int vibrate_permission = ContextCompat.checkSelfPermission(mActivity, VIBRATE);
        return camera_permission == PackageManager.PERMISSION_GRANTED && vibrate_permission == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        // this method is to request
        // the runtime permission.
        int PERMISSION_REQUEST_CODE = 200;
        ActivityCompat.requestPermissions(mActivity, new String[]{CAMERA, VIBRATE}, PERMISSION_REQUEST_CODE);
    }

    public JSONObject GetTicketIdDetailsAPI(String url, String json) {
        String Header_id,Header_value;
        Header_id = "Authorization";
        Header_value = "Baerer "+ GlobalVariables.user_token;
        Log.d("URL:",url);
        JSONObject jsonObjectResp = null;


        try {

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, json);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .method("GET",null)
                    .addHeader(Header_id, Header_value)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                jsonObjectResp = GetTicketIdDetailsResponse(networkResp);
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            jsonObjectResp = GetTicketIdDetailsResponse(err);
        }

        return jsonObjectResp;
    }

    public JSONObject GetTicketIdDetailsResponse(final String str) {
        //dialogshow.dismiss();

        get_reservation_id_response = null;
        try {
            get_reservation_id_response = new JSONObject(str);

            Log.e("Ticket API Response=>", String.valueOf(get_reservation_id_response));
            // String status= response.getString("status");
            String message = get_reservation_id_response.getString("msg");
            int code = get_reservation_id_response.getInt("code");


            mActivity.runOnUiThread(() -> {
                if (code == 20){
                    try {

                        JSONObject json = new JSONObject(str);

                        JSONObject data = json.getJSONObject("data");

                        String order_id = data.getString("id");
                        JSONObject reservations = data.getJSONObject("reservations");
                        String pass_id = reservations.getString("pass_id");

                        JSONObject vas_order_status_types = data.getJSONObject("vas_order_status_types");
                        String id = vas_order_status_types.getString("id");
                        String name = vas_order_status_types.getString("name");
                        Log.e("ID",id);
                        Log.e("ID",name);
                        if (id != null && id.equals("3")) {
                            ViewDialog alert = new ViewDialog();
                            alert.showDialog(mActivity, "The Order is Completed");
                            GlobalVariables.progressDialog.dismiss();
                            return;
                        }

                        GlobalVariables.order_item_id = order_id;


                        Intent i = new Intent(mActivity, OrderDetailsActivity.class);
                        i.putExtra("pass_id",pass_id);
                        mActivity.startActivity(i);


                        camera.stopScanner();
                        dialog.dismiss();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                else {
                    AlertDialog.Builder alert = new AlertDialog.Builder(mActivity);
                    alert.setMessage("Invalid Ticket Number").setPositiveButton("Close", null);
                    AlertDialog alert1 = alert.create();
                    alert1.show();
                }


            });



        } catch (Exception ex) {
            //  Log.e("Could not parse malformed JSON: \"" + json + "\"");
            try {
                get_reservation_id_response = new JSONObject();
                get_reservation_id_response.put("result", "failed");
                get_reservation_id_response.put("data", str);
                get_reservation_id_response.put("error", ex.getMessage());


                // tv_response.setText("ID:"+id+" and Name:"+name);
                Toast.makeText(mActivity, "" + ex.getMessage(), Toast.LENGTH_SHORT).show();


            } catch (Exception exx) {
            }
        }
        GlobalVariables.progressDialog.dismiss();
        return get_reservation_id_response;
    }
}

