package com.sps.shuttelserviceapp;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.sps.shuttelserviceapp.Utils.GlobalVariables;
import com.sps.shuttelserviceapp.Utils.ViewDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class OrderDetailsActivity extends AppCompatActivity implements View.OnClickListener {
    JSONObject OrderDetailsResponse, ConfirmButtonResponse, confirm_button_response;
    TextView tv_order_id, tv_site_name, tv_service_type, tv_service_sub_type, tv_status, tv_payment_status, tv_no_of_passenger, tv_amount, tv_customer_name, tv_contact_number, tv_order_time;
    Button btn_confirm, confirm_button;
    ImageView iv_close;
    AlertDialog alert;
    String OrderID = "",full_name;
    int count = 0;
    int noOfPassenger = 1 ;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_details);

        tv_order_id = findViewById(R.id.tv_order_id);
        tv_site_name = findViewById(R.id.tv_site_name);
        tv_service_type = findViewById(R.id.tv_service_type);
        tv_service_sub_type = findViewById(R.id.tv_service_sub_type);
        tv_status = findViewById(R.id.tv_status);
        tv_payment_status = findViewById(R.id.tv_payment_status);
        tv_amount = findViewById(R.id.tv_amount);
        tv_customer_name = findViewById(R.id.tv_customer_name);
        tv_contact_number = findViewById(R.id.tv_contact_number);
        tv_order_time = findViewById(R.id.tv_order_time);

        String passid = getIntent().getStringExtra("pass_id");

        tv_no_of_passenger = findViewById(R.id.tv_no_of_passenger);
        tv_no_of_passenger.setOnClickListener(v -> {

        });


        confirm_button = findViewById(R.id.confirm_button);
        /*if (GlobalVariables.order_status_id.equals("1")) {
            Log.e("Order Status", GlobalVariables.order_status_id);
            confirm_button.setVisibility(View.VISIBLE);
        } else {
            confirm_button.setVisibility(View.GONE);
        }*/
        confirm_button.setOnClickListener(this);

        iv_close = findViewById(R.id.iv_close);
        iv_close.setOnClickListener(v -> finish());


        //Get the Order Details
        if (passid != null) {
            String url = GlobalVariables.API_URL + "cashier/reservations-by-pass-id/" + passid;
            Thread thread = new Thread(() -> {
                try {
                    GetOrderDetails(url, "");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            thread.start();
        } else {
            String url = GlobalVariables.API_URL + "cashier/reservations-by-pass-id/" + GlobalVariables.ticket_number;
            Thread thread = new Thread(() -> {
                try {
                    GetOrderDetails(url, "");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            thread.start();
        }

    }

    public JSONObject GetOrderDetails(String url, String json) {
        String Header_id, Header_value;
        Header_id = "Authorization";
        Header_value = "Bearer " + GlobalVariables.user_token;
        Log.e("User token", Header_value);
        Log.e("Order details URL", url);
        JSONObject jsonObjectResp = null;

        try {
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, json);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .method("GET", null)
                    .addHeader(Header_id, Header_value)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                jsonObjectResp = GetOrderDetailsResponse(networkResp);
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            jsonObjectResp = GetOrderDetailsResponse(err);
        }
        return jsonObjectResp;
    }

    private JSONObject GetOrderDetailsResponse(final String str) {
        OrderDetailsResponse = null;
        try {
            OrderDetailsResponse = new JSONObject(str);
            Log.e("Order Details Response", String.valueOf(OrderDetailsResponse));
            String message = OrderDetailsResponse.getString("msg");
            int code = OrderDetailsResponse.getInt("code");

            runOnUiThread(() -> {

                if (code == 20) {
                    try {
                        JSONObject json = new JSONObject(str);

                        JSONObject jsonObject = json.getJSONObject("data");
                        JSONObject result = jsonObject.getJSONObject("result");
                        String order_id1 = result.getString("id");
                        tv_order_id.setText("# " + order_id1);
                        OrderID = order_id1;
                        JSONObject sites = result.getJSONObject("sites");
                        String site_name = sites.getString("name");
                        tv_site_name.setText(site_name);
                      //  String amount = result.getString("amount");

                        JSONObject payment_status = result.getJSONObject("payment_status");
                        String name = payment_status.getString("name");
                        tv_payment_status.setText(name);

                        String start_time = result.getString("start_time");
                        if (start_time.equals("") || start_time.equals("null")) {
                            tv_order_time.setText("----");
                        } else {
                            SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                            input.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
                            SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy hh:mm aa");

                            Date date_time = null;
                            try {
                                date_time = input.parse(start_time);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            String format_date_time = output.format(date_time);

                            tv_order_time.setText(format_date_time);
                        }


                        JSONArray jsonArray = result.getJSONArray("vas_orders");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            String order_id = jsonObject1.getString("id");
                            tv_order_id.setText("# " + order_id);
                            OrderID = order_id;
                            JSONObject vas_types = jsonObject1.getJSONObject("vas_types");
                            String service_type = vas_types.getString("name");
                            tv_service_type.setText(service_type);
                            JSONObject vas_order_status_types = jsonObject1.getJSONObject("vas_order_status_types");
                            String status = vas_order_status_types.getString("name");
                            tv_status.setText(status);
                            String qty = jsonObject1.getString("qty");
                            noOfPassenger = Integer.parseInt(qty);
                            tv_no_of_passenger.setText(qty);
                            JSONObject vas_sub_type_obj = jsonObject1.getJSONObject("vas_sub_types");
                            String service_sub_type = vas_sub_type_obj.getString("name");
                            tv_service_sub_type.setText(service_sub_type);
                            String amount = jsonObject1.getString("amount");
                            tv_amount.setText(amount + " SAR");
                        }


                            if (result.has("customers") && !result.isNull("customers")) {
                                Log.e("Checking", ("Customer"));
                                JSONObject customers = result.getJSONObject("customers");
                                String first_name = customers.getString("full_name");
                                String mobile = customers.getString("mobile");
                                if (customers.has("country_code") && !customers.isNull("country_code")){
                                    JSONObject country_code_obj = customers.getJSONObject("country_code");
                                    String dial_code = country_code_obj.getString("dial_code");
                                    tv_customer_name.setText(first_name);
                                    tv_contact_number.setText(dial_code + " " +mobile);
                                }else{
                                    tv_customer_name.setText(first_name);
                                    tv_contact_number.setText("+966 " + mobile);
                                }
                            }else{
                                Log.e("Checking", "Guest");
                                if (result.has("guest_users") && !result.isNull("guest_users")) {
                                    JSONObject guest_users = result.getJSONObject("guest_users");
                                    String first_name = guest_users.getString("full_name");
                                    String last_name = guest_users.getString("last_name");
                                    if(last_name.equals("") || last_name.equals("null")){
                                        full_name = first_name;
                                    } else {
                                        full_name = first_name + " " + last_name;
                                    }
                                    String mobile = guest_users.getString("mobile");
                                    if (guest_users.has("country_code") && !guest_users.isNull("country_code")) {
                                        JSONObject country_code_obj = guest_users.getJSONObject("country_code");
                                        String dial_code = country_code_obj.getString("dial_code");
                                        tv_customer_name.setText(full_name);
                                        tv_contact_number.setText(dial_code + " " + mobile);
                                    } else {
                                        tv_customer_name.setText(full_name);
                                        tv_contact_number.setText("+966 " + mobile);
                                    }
                                }
                            }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (code == 43) {
                    SharedPreferences prefs = getSharedPreferences(
                            "saved_data", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("login_status", "");
                    editor.putString("token", "");
                    editor.putString("role_id", "");
                    editor.putString("user_name", "");
                    editor.apply();
                    Toast.makeText(OrderDetailsActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(OrderDetailsActivity.this, LoadingActivity.class));
                    finish();

                } else {
                    ViewDialog alert = new ViewDialog();
                    alert.showDialog(OrderDetailsActivity.this, message);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.confirm_button:
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                LayoutInflater inflater = this.getLayoutInflater();

                View dialogView = inflater.inflate(R.layout.activity_number_of_passenger_dialog_box, null);
                builder.setView(dialogView);

                count = noOfPassenger;
                alert = builder.create();
                alert.show();

                TextView tv_display_passenger = dialogView.findViewById(R.id.tv_display_passenger);
                Button ig_increment = (Button) dialogView.findViewById(R.id.btn_inc);
                Button ig_decrement = (Button) dialogView.findViewById(R.id.btn_dec);
                Button dialog_btn_confirm = dialogView.findViewById(R.id.btn_confirm);
                tv_display_passenger.setText("" + noOfPassenger);

                ig_increment.setOnClickListener(v12 -> {
                    if (count < 10){
                        count++;
                    }else{

                    }
                    tv_display_passenger.setText("" + count);
                });

                ig_decrement.setOnClickListener(v13 -> {
                    if (count <= 1) {
                        count = 1;
                        tv_display_passenger.setText("" + count);
                    } else {
                        count--;
                        tv_display_passenger.setText("" + count);
                    }
                });

                dialog_btn_confirm.setOnClickListener(v1 -> {
                    alert.dismiss();
                    if (OrderID.equals("")){
                    Toast.makeText(this,"Incorrect ID", Toast.LENGTH_SHORT).show();
                }else{
                    String url = GlobalVariables.API_URL + "worker/status/" + OrderID;
                    String confirm_order_input = "{\n" +
                            "  \"status\": \"" + "accept" + "\",\n" +
                            "  \"qty\": \" "+ count + "\"\n" +
                            "}";
                    Log.e("Confirm Order URL", url);
                    Log.e("Confirm Order input",confirm_order_input);

                    Thread thread = new Thread(() -> {
                        try {
                            ConfirmButton(url, confirm_order_input);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    });
                    thread.start();
                }
                });
        }
    }

    private JSONObject ConfirmButton(String url, String json) {
        String Header_id, Header_value;
        Header_id = "Authorization";
        Header_value = "Bearer " + GlobalVariables.user_token;
        JSONObject jsonObjectResp = null;
        try {
            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, json);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .method("PATCH", body)
                    .addHeader(Header_id, Header_value)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                jsonObjectResp = GetConfirmButtonResponse(networkResp);
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            jsonObjectResp = GetConfirmButtonResponse(err);
        }
        return jsonObjectResp;
    }

    private JSONObject GetConfirmButtonResponse(final String str) {
        ConfirmButtonResponse = null;

        try {
            ConfirmButtonResponse = new JSONObject(str);
            Log.e("Confirm Order Response ", String.valueOf(ConfirmButtonResponse));
            String message = OrderDetailsResponse.getString("msg");
            int code = OrderDetailsResponse.getInt("code");

            runOnUiThread(() -> {

                if (code == 20) {
                    Toast.makeText(OrderDetailsActivity.this, "You Confirmed Successfully", Toast.LENGTH_SHORT).show();
                    Intent submit = new Intent(OrderDetailsActivity.this, OrderListActivity.class);
                    startActivity(submit);
                    finish();
                } else if (code == 43) {
                    SharedPreferences prefs = getSharedPreferences(
                            "saved_data", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("login_status", "");
                    editor.putString("token", "");
                    editor.putString("role_id", "");
                    editor.putString("user_name", "");
                    editor.apply();
                    Toast.makeText(OrderDetailsActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(OrderDetailsActivity.this, LoadingActivity.class));
                    finish();

                } else {
                    ViewDialog alert = new ViewDialog();
                    alert.showDialog(OrderDetailsActivity.this, message);
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public JSONObject GetDialogConfirmButton(String url, String json) {
        String Header_id, Header_value;
        Header_id = "Authorization";
        Header_value = "Bearer " + GlobalVariables.user_token;
        Log.e("Change Status URL", url);
        JSONObject jsonObjectResp = null;

        try {

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, json);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .method("PATCH", body)
                    .addHeader(Header_id, Header_value)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                jsonObjectResp = GetDialogConfirmButtonResponse(networkResp);
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            jsonObjectResp = GetDialogConfirmButtonResponse(err);
        }

        return jsonObjectResp;
    }

    private JSONObject GetDialogConfirmButtonResponse(final String str) {
        confirm_button_response = null;
        try {
            confirm_button_response = new JSONObject(str);
            String message = confirm_button_response.getString("msg");
            int code = confirm_button_response.getInt("code");

            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (code == 20) {
                    } else {
                        ViewDialog alert = new ViewDialog();
                        alert.showDialog(OrderDetailsActivity.this, message);
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return confirm_button_response;
    }
}