package com.sps.shuttelserviceapp;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.sps.shuttelserviceapp.Controllers.HistoryListController;
import com.sps.shuttelserviceapp.InternetCheck.ConnectionReceiver;
import com.sps.shuttelserviceapp.Models.HistoryModel;
import com.sps.shuttelserviceapp.Utils.GlobalVariables;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;


public class HistoryFragment extends Fragment implements ConnectionReceiver.ReceiverListener{
    boolean isConnected;
    JSONObject history_list_response;
    private ArrayList<HistoryModel> history_list = new ArrayList<>();
    HistoryListController history_adapter;
    RecyclerView rv_history_list;
    RecyclerView.LayoutManager RecyclerViewLayoutManager;
    // Linear Layout Manager
    LinearLayoutManager HorizontalLayout;
    //    EditText etsearch;

    private String mParam1;
    private String mParam2;
    SwipeRefreshLayout swipeRefreshLayout;
    String url;

    public HistoryFragment() {

    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        rv_history_list = (RecyclerView) getView().findViewById(R.id.rv_history);
        RecyclerViewLayoutManager = new LinearLayoutManager(getActivity());
        rv_history_list.setLayoutManager(RecyclerViewLayoutManager);
        HorizontalLayout = new LinearLayoutManager(getActivity());
        rv_history_list.setLayoutManager(HorizontalLayout);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL);
        rv_history_list.addItemDecoration(dividerItemDecoration);
        history_adapter = new HistoryListController(getActivity(), history_list);
        rv_history_list.setAdapter(history_adapter);

        swipeRefreshLayout = (SwipeRefreshLayout)getView().findViewById(R.id.refresh_layout);

        checkConnection();
        if(isConnected){
            url = GlobalVariables.API_URL + "supervisor/history/?vas_type_id=3&offset=0&limit=100";

            GlobalVariables.progressDialog = GlobalVariables.createProgressDialog(getActivity());
            GlobalVariables.progressDialog.show();

            Thread thread = new Thread(new Runnable() {

                @Override
                public void run() {
                    try {

                        GetHistoryList(url, "");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            });
            thread.start();
        }else{

            Log.e("Checking 2 ", String.valueOf(isConnected));
            Toast.makeText(getActivity(),"Please Check your Internet Connection !",Toast.LENGTH_LONG).show();

        }


        swipeRefreshLayout.setOnRefreshListener(() -> {
            url = GlobalVariables.API_URL + "supervisor/history/?vas_type_id=3&offset=0&limit=100";

            GlobalVariables.progressDialog = GlobalVariables.createProgressDialog(getActivity());
            GlobalVariables.progressDialog.show();

            Thread thread1 = new Thread(new Runnable() {

                @Override
                public void run() {
                    try {

                        GetHistoryList(url, "");

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

            });
            thread1.start();
            swipeRefreshLayout.setRefreshing(false);
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_history, container, false);
    }

    public JSONObject GetHistoryList(String url, String json) {
        String Header_id,Header_value;
        Header_id = "Authorization";
        Header_value = "Bearer "+ GlobalVariables.user_token;
        Log.e("History URL",url);
        JSONObject jsonObjectResp = null;

        try {

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, json);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .method("GET",null)
                    .addHeader(Header_id, Header_value)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                jsonObjectResp = GetHistoryListResponse(networkResp);
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            jsonObjectResp = GetHistoryListResponse(err);
        }

        return jsonObjectResp;
    }

    public JSONObject GetHistoryListResponse(final String str) {
        history_list.clear();
        history_list_response = null;
        try {
            history_list_response = new JSONObject(str);

            Log.e("Pending List=>", String.valueOf(history_list_response));

            String message = history_list_response.getString("msg");
            int code = history_list_response.getInt("code");

            getActivity().runOnUiThread(() -> {

                if (code == 20) {

                    try {
                        JSONObject json = new JSONObject(str);

                        JSONArray jsonArray = json.getJSONArray("data");


                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);

                            String history_date = jsonObject.getString("date");
                            JSONArray history_array = jsonObject.getJSONArray("data");
                            for (int j = 0; j < history_array.length(); j++) {
                                JSONObject history_jsonObject = history_array.getJSONObject(j);
                                String history_item_id = history_jsonObject.getString("id");
                                String history_item_start_time = history_jsonObject.getString("start_time");
                                String history_item_end_time = history_jsonObject.getString("end_time");

                                JSONObject vas_types_name = history_jsonObject.getJSONObject("vas_types");
                                String vas_type_id = vas_types_name.getString("id");
                                String vas_type_name = vas_types_name.getString("name");
                                Log.e("Vas type name", vas_type_name);

                                JSONObject vas_order_status_types = history_jsonObject.getJSONObject("vas_order_status_types");
                                String vas_type_status = vas_order_status_types.getString("name");
                                Log.e("vas_order_status_types", vas_type_status);
                                HistoryModel historyModel = new HistoryModel();

                                if(j == 0){
                                    historyModel.setHistory_date(history_date);
                                    historyModel.setHistory_item_id(history_item_id);
                                    historyModel.setHistory_item_start_date(history_item_start_time);
                                    historyModel.setHistory_item_end_date(history_item_end_time);
                                    historyModel.setHistory_item_service_name(vas_type_name);
                                    historyModel.setHistory_vas_id(vas_type_id);
                                }else{
                                    historyModel.setHistory_date("");
                                    historyModel.setHistory_item_id(history_item_id);
                                    historyModel.setHistory_item_start_date(history_item_start_time);
                                    historyModel.setHistory_item_end_date(history_item_end_time);
                                    historyModel.setHistory_item_service_name(vas_type_name);
                                    historyModel.setHistory_vas_id(vas_type_id);
                                }

                                history_list.add(historyModel);
                            }

                        }
//
                        history_adapter.setHistoryListDetails(history_list);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                }
                else if (code == 43) {
                    SharedPreferences prefs = getActivity().getSharedPreferences(
                            "saved_data", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("login_status", "");
                    editor.putString("token", "");
                    editor.putString("role_id", "");
                    editor.putString("user_name", "");
                    editor.apply();
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getActivity(), LoadingActivity.class));

                }
                else {
                    //  Toast.makeText(DisplayParkingActivity.this, "" + message, Toast.LENGTH_SHORT).show();
                }

                Log.e("Checking=>","");
                GlobalVariables.progressDialog.dismiss();

            });


        } catch (Exception ex) {
            //  Log.e("Could not parse malformed JSON: \"" + json + "\"");
            try {
                history_list_response = new JSONObject();
                history_list_response.put("result", "failed");
                history_list_response.put("data", str);
                history_list_response.put("error", ex.getMessage());
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        // tv_response.setText("ID:"+id+" and Name:"+name);
                        Toast.makeText(getActivity(), "" + ex.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
            } catch (Exception exx) {
            }
        }

        return history_list_response;

    }

    private void filter(String text) {
        // creating a new array list to filter our data.
        ArrayList<HistoryModel> filteredlist = new ArrayList<HistoryModel>();

        // running a for loop to compare elements.
        for (HistoryModel item : history_list) {
            // checking if the entered string matched with any item of our recycler view.
            if (item.getHistory_item_service_name().toLowerCase().contains(text.toLowerCase())) {
                // if the item is matched we are
                // adding it to our filtered list.
                filteredlist.add(item);
            }
        }
        if (filteredlist.isEmpty()) {
            // if no item is added in filtered list we are
            // displaying a toast message as no data found.
            // Toast.makeText(this, "No Data Found..", Toast.LENGTH_SHORT).show();
        } else {
            // at last we are passing that filtered
            // list to our adapter class.
            history_adapter.setHistoryListDetails(filteredlist);
        }
    }

    private void checkConnection() {

        // initialize intent filter
        IntentFilter intentFilter = new IntentFilter();

        // add action
        intentFilter.addAction("android.new.conn.CONNECTIVITY_CHANGE");

        // register receiver
        getActivity().registerReceiver(new ConnectionReceiver(), intentFilter);

        // Initialize listener
        ConnectionReceiver.Listener = this;

        // Initialize connectivity manager
        ConnectivityManager manager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        // Initialize network info
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

        // get connection status
        isConnected = networkInfo != null && networkInfo.isConnectedOrConnecting();


    }

    @Override
    public void onNetworkChange(boolean isConnected) {

    }
}