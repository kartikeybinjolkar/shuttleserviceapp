package com.sps.shuttelserviceapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sps.shuttelserviceapp.InternetCheck.ConnectionReceiver;
import com.sps.shuttelserviceapp.Utils.GlobalVariables;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class ProfileFragment extends Fragment implements View.OnClickListener, ConnectionReceiver.ReceiverListener{

    boolean isConnected;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    JSONObject profile_API_response,update_profile_response;
    EditText et_username,et_user_email,et_user_phone_number;
    TextView tv_last_login_at;
    LinearLayout ll_save_btn_disable,ll_save_btn_enable;
    Button btn_cancel,btn_logout,btn_save;
    JSONObject logout_api_response;
    String country_code_id;

    private String mParam1;
    private String mParam2;

    public ProfileFragment() {

    }

    public static ProfileFragment newInstance(String param1, String param2) {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        et_username = (EditText) getView().findViewById(R.id.et_user_name);
        et_user_email = (EditText) getView().findViewById(R.id.et_user_email);
        tv_last_login_at = (TextView) getView().findViewById(R.id.tv_last_login_at);
        et_user_phone_number = (EditText) getView().findViewById(R.id.et_user_phone_number);
//        iv_edit = (ImageView) getView().findViewById(R.id.iv_edit);
        ll_save_btn_disable = (LinearLayout) getView().findViewById(R.id.ll_save_btn_disable);
        ll_save_btn_enable = (LinearLayout) getView().findViewById(R.id.ll_save_btn_enable);
        btn_cancel = (Button) getView().findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(this);

        btn_logout = (Button) getView().findViewById(R.id.btn_logout);
        btn_logout.setOnClickListener(this);

        btn_save = (Button) getView().findViewById(R.id.btn_save);
        btn_save.setOnClickListener(this);

        et_username.setEnabled(false);
        et_user_email.setEnabled(false);
        et_user_phone_number.setEnabled(false);
        ll_save_btn_enable.setVisibility(View.GONE);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        checkConnection();
        if(isConnected){
            String url = GlobalVariables.API_URL + "supervisor/profile";

            GlobalVariables.progressDialog = GlobalVariables.createProgressDialog(getActivity());
            GlobalVariables.progressDialog.show();

            Thread thread = new Thread(() -> {
                try {
                    GetProfileData(url, "");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            thread.start();
        }else{
            Log.e("Checking 2 ", String.valueOf(isConnected));
            Toast.makeText(getActivity(),"Please Check your Internet Connection !",Toast.LENGTH_LONG).show();
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    public JSONObject GetProfileData(String url, String json) {
        String Header_id,Header_value;
        Header_id = "Authorization";
        Header_value = "Bearer "+ GlobalVariables.user_token;
        Log.e("History URL",url);
        JSONObject jsonObjectResp = null;

        try {

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, json);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .method("GET",null)
                    .addHeader(Header_id, Header_value)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                jsonObjectResp = GetProfileDataResponse(networkResp);
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            jsonObjectResp = GetProfileDataResponse(err);
        }

        return jsonObjectResp;
    }
    public JSONObject GetProfileDataResponse(final String str) {

        profile_API_response = null;
        try {
            profile_API_response = new JSONObject(str);

            Log.e("History Response", String.valueOf(profile_API_response));
            String message = profile_API_response.getString("msg");
            int code = profile_API_response.getInt("code");

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (code == 20) {

                        try {
                            JSONObject json = new JSONObject(str);

                            JSONObject jsonObject = json.getJSONObject("data");
                            String mobile = jsonObject.getString("mobile");
                            String last_login_at = jsonObject.getString("last_login_at");
                            String email = jsonObject.getString("email");
                            String name = jsonObject.getString("name");
                            if (last_login_at.equals("")||last_login_at.equals(null))
                            {
                                tv_last_login_at.setText("------");
                            }else {
                                SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                                input.setTimeZone(TimeZone.getTimeZone("Etc/UTC"));
                                SimpleDateFormat output = new SimpleDateFormat("dd/MM/yyyy hh:mm aa");

                                Date date_time = null;
                                try {
                                    date_time = input.parse(last_login_at);
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                String format_date_time = output.format(date_time);

                                tv_last_login_at.setText(format_date_time);
                            }
                            if (jsonObject.has("country_code") && !jsonObject.isNull("country_code")) {
                                JSONObject country_code = jsonObject.getJSONObject("country_code");
                                country_code_id = country_code.getString("dial_code");
                            }else{
                            }
                            et_username.setText(name);
                            et_user_email.setText(email);
                            et_user_phone_number.setText(country_code_id+" - "+mobile);
                            Log.e("Email",email);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                    else if (code == 43) {
                        SharedPreferences prefs = getActivity().getSharedPreferences(
                                "saved_data", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("login_status", "");
                        editor.putString("token", "");
                        editor.putString("role_id", "");
                        editor.putString("user_name", "");
                        editor.apply();
                        Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getActivity(), LoadingActivity.class));
                    }
                    else {
//                        ViewDialog alert = new ViewDialog();
//                        alert.showDialog(HistoryActivity.this, "Minimum 8 numbers required");
                        Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
                    }
                    Log.e("Checking=>","");
                    GlobalVariables.progressDialog.dismiss();
                }
            });
        } catch (Exception ex) {
            //  Log.e("Could not parse malformed JSON: \"" + json + "\"");
            try {
                profile_API_response = new JSONObject();
                profile_API_response.put("result", "failed");
                profile_API_response.put("data", str);
                profile_API_response.put("error", ex.getMessage());
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // tv_response.setText("ID:"+id+" and Name:"+name);
                        Toast.makeText(getActivity(), "" + ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
            } catch (Exception exx) {
            }
        }
        return profile_API_response;
    }
    public JSONObject UpdateProfileAPI(String url, String json) {
        String Header_id,Header_value;
        Header_id = "Authorization";
        Header_value = "Baerer "+ GlobalVariables.user_token;
        Log.e("Update URL",url);
        JSONObject jsonObjectResp = null;
        try {

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, json);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .method("PATCH",body)
                    .addHeader(Header_id, Header_value)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                jsonObjectResp = UpdateProfileAPIResponse(networkResp);
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            jsonObjectResp = UpdateProfileAPIResponse(err);
        }

        return jsonObjectResp;
    }
    public JSONObject UpdateProfileAPIResponse(final String str) {

        update_profile_response = null;
        try {
            update_profile_response = new JSONObject(str);

            Log.e("Update Profile Response", String.valueOf(update_profile_response));
            String message = update_profile_response.getString("msg");
            int code = update_profile_response.getInt("code");

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    if (code == 20) {

                        try {
                            JSONObject json = new JSONObject(str);
                            Toast.makeText(getActivity(), "Profile Updated Successfully", Toast.LENGTH_SHORT).show();
                            ProfileFragment fragment1 = new ProfileFragment();
                            FragmentManager fragmentManager = getFragmentManager();
                            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(android.R.id.content, fragment1);
                            fragmentTransaction.commit();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                    else if (code == 43) {
                        SharedPreferences prefs = getActivity().getSharedPreferences(
                                "saved_data", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("login_status", "");
                        editor.putString("token", "");
                        editor.putString("role_id", "");
                        editor.putString("user_name", "");
                        editor.apply();
                        Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(getActivity(), LoadingActivity.class));

                    }
                    else {
//                        ViewDialog alert = new ViewDialog();
//                        alert.showDialog(HistoryActivity.this, "Minimum 8 numbers required");
                        Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
                    }

                    Log.e("Checking=>","");
                    GlobalVariables.progressDialog.dismiss();

                }
            });


        } catch (Exception ex) {
            //  Log.e("Could not parse malformed JSON: \"" + json + "\"");
            try {
                update_profile_response = new JSONObject();
                update_profile_response.put("result", "failed");
                update_profile_response.put("data", str);
                update_profile_response.put("error", ex.getMessage());
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        // tv_response.setText("ID:"+id+" and Name:"+name);
                        Toast.makeText(getActivity(), "" + ex.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
            } catch (Exception exx) {
            }
        }

        return update_profile_response;

    }

    private void showPopup() {
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setMessage("Are you sure want to logout?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener()                 {

                    public void onClick(DialogInterface dialog, int which) {



                        String resent_otp_url = GlobalVariables.API_URL + "users/logout";


                        GlobalVariables.progressDialog = GlobalVariables.createProgressDialog(getActivity());
                        GlobalVariables.progressDialog.show();

                        Thread thread = new Thread(new Runnable() {

                            @Override
                            public void run() {
                                try {
                                    LogOutAPI(resent_otp_url, "");

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        });
                        thread.start();




                    }
                }).setNegativeButton("NO", null);

        AlertDialog alert1 = alert.create();
        alert1.show();
    }

    public JSONObject LogOutAPI(String url, String json) {


        Log.e("send_otp_url=>",url);
        Log.e("send_otp_json=>",json);
        String Header_id,Header_value;
        Header_id = "Authorization";
        Header_value = "Bearer "+ GlobalVariables.user_token;


        JSONObject jsonObjectResp = null;

        try {

            MediaType JSON = MediaType.parse("application/json; charset=utf-8");

            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, json);
            okhttp3.Request request = new okhttp3.Request.Builder()
                    .url(url)
                    .method("PATCH",body)
                    .addHeader(Header_id, Header_value)
                    .build();

            okhttp3.Response response = client.newCall(request).execute();

            String networkResp = response.body().string();
            if (!networkResp.isEmpty()) {
                jsonObjectResp = LogOutAPIResponse(networkResp);
            }
        } catch (Exception ex) {
            String err = String.format("{\"result\":\"false\",\"msg\":\"%s\"}", ex.getMessage());
            jsonObjectResp = LogOutAPIResponse(err);
        }

        return jsonObjectResp;
    }
    public JSONObject LogOutAPIResponse(final String str) {
        //dialogshow.dismiss();
        String id, name;
        logout_api_response = null;
        try {
            logout_api_response = new JSONObject(str);
            Log.e("resend OTP Response=>", String.valueOf(logout_api_response));

            String message = logout_api_response.getString("msg");
            int code = logout_api_response.getInt("code");


            getActivity().runOnUiThread(() -> {

                if (code == 20) {

                    SharedPreferences prefs = getActivity().getSharedPreferences(
                            "saved_data", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("login_status", "");
                    editor.putString("token", "");
                    editor.putString("role_id", "");
                    editor.putString("user_name", "");
                    editor.apply();
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getActivity(), LoadingActivity.class));
                    getActivity().finish();



                }
                if (code == 43) {
                    SharedPreferences prefs = getActivity().getSharedPreferences(
                            "saved_data", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = prefs.edit();
                    editor.putString("login_status", "");
                    editor.putString("token", "");
                    editor.putString("role_id", "");
                    editor.putString("user_name", "");
                    editor.apply();
                    Toast.makeText(getActivity(), "" + message, Toast.LENGTH_SHORT).show();
                    startActivity(new Intent(getActivity(), LoadingActivity.class));
                    getActivity().finish();

                }
//                    else {
//                        ViewDialog alert = new ViewDialog();
//                        alert.showDialog(getActivity(), message);
//                        //Toast.makeText(MainActivity.this, "" + message, Toast.LENGTH_SHORT).show();
//                    }

                GlobalVariables.progressDialog.dismiss();

            });


        } catch (Exception ex) {
            //  Log.e("Could not parse malformed JSON: \"" + json + "\"");
            try {
                logout_api_response = new JSONObject();
                logout_api_response.put("result", "failed");
                logout_api_response.put("data", str);
                logout_api_response.put("error", ex.getMessage());
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        // tv_response.setText("ID:"+id+" and Name:"+name);
                        Toast.makeText(getActivity(), "" + ex.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
            } catch (Exception exx) {
            }
            GlobalVariables.progressDialog.dismiss();
        }

        return logout_api_response;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:

                ll_save_btn_enable.setVisibility(View.GONE);
                ll_save_btn_disable.setVisibility(View.VISIBLE);
                et_username.setEnabled(false);
                et_user_email.setEnabled(false);
                et_user_phone_number.setEnabled(false);
                break;

            case R.id.btn_save:

                String url = GlobalVariables.API_URL + "supervisor/profile";

                String update_profile_input_json = "{\n" +
                        "  \"mobile\": \""+et_user_phone_number.getText().toString()+"\",\n" +
                        "  \"name\": \""+et_username.getText().toString()+"\",\n" +
                        "  \"email\": \""+et_user_email.getText().toString()+"\"\n" +
                        "}";

                GlobalVariables.progressDialog = GlobalVariables.createProgressDialog(getActivity());
                GlobalVariables.progressDialog.show();

                Thread thread = new Thread(new Runnable() {

                    @Override
                    public void run() {
                        try {

                            UpdateProfileAPI(url, update_profile_input_json);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                });
                thread.start();
                break;

            case R.id.btn_logout:
                showPopup();
                break;
        }
    }
    private void checkConnection() {

        // initialize intent filter
        IntentFilter intentFilter = new IntentFilter();

        // add action
        intentFilter.addAction("android.new.conn.CONNECTIVITY_CHANGE");

        // register receiver
        getActivity().registerReceiver(new ConnectionReceiver(), intentFilter);

        // Initialize listener
        ConnectionReceiver.Listener = this;

        // Initialize connectivity manager
        ConnectivityManager manager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);

        // Initialize network info
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

        // get connection status
        isConnected = networkInfo != null && networkInfo.isConnectedOrConnecting();


    }
    @Override
    public void onNetworkChange(boolean isConnected) {

    }
}